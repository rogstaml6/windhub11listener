#tag Class
Protected Class InspectionDamagesClass
Inherits GenInterfDBClass
	#tag Property, Flags = &h0
		inspectiondamages_area As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_damage_desc As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_damage_num As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_damage_radius As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_damage_severity As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_damage_type As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_inspection_num As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_length As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_photo_desc01 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_photo_desc02 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_position_edge As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_position_side As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_project_number As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_remark As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_section As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_updatable As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		inspectiondamages_web As String
	#tag EndProperty

	#tag Property, Flags = &h0
		last_synced As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		mobile_timestamp As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		unique_record_identifier As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_area"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_damage_desc"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_damage_num"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_damage_radius"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_damage_severity"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_damage_type"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_inspection_num"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_length"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_photo_desc01"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_photo_desc02"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_position_edge"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_position_side"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_project_number"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_remark"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_section"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_updatable"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspectiondamages_web"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="last_synced"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mobile_timestamp"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="unique_record_identifier"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
