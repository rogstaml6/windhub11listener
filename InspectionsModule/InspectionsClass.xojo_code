#tag Class
Protected Class InspectionsClass
Inherits GenInterfDBClass
	#tag Property, Flags = &h0
		inspections_access_method As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_assignment_number As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_blade_length As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_blade_manufacturer As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_blade_num As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_blade_serial As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_end_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_inspection_num As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_int_ext As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_priority As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_progress_status As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_radius1 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_radius1_ps As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_radius1_ss As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_radius2 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_radius2_ps As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_radius2_ss As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_radius3 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_radius3_ps As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_radius3_ss As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_radius_drain_cleared As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_radius_drain_value As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_remark As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_start_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_time_estimated As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_time_spent As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspections_wtg_num As String
	#tag EndProperty

	#tag Property, Flags = &h0
		last_synced As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		mobile_timestamp As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		unique_record_identifier As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_access_method"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_assignment_number"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_blade_length"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_blade_manufacturer"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_blade_num"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_blade_serial"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_end_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_inspection_num"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_int_ext"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_priority"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_progress_status"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_radius1"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_radius1_ps"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_radius1_ss"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_radius2"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_radius2_ps"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_radius2_ss"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_radius3"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_radius3_ps"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_radius3_ss"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_radius_drain_cleared"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_radius_drain_value"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_remark"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_start_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_time_estimated"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_time_spent"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspections_wtg_num"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="last_synced"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mobile_timestamp"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="unique_record_identifier"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
