#tag Class
Protected Class RepairStepsClass
Inherits GenInterfDBClass
	#tag Property, Flags = &h0
		last_synced As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		mobile_timestamp As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		repairsteps_assignment_number As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairsteps_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairsteps_photodesc01 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairsteps_repair_num As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairsteps_step_num As String
	#tag EndProperty

	#tag Property, Flags = &h0
		unique_record_identifier As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="last_synced"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mobile_timestamp"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairsteps_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairsteps_photodesc01"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairsteps_repair_num"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairsteps_step_num"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="unique_record_identifier"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairsteps_assignment_number"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
