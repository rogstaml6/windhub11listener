#tag Class
Protected Class RepairsClass
Inherits GenInterfDBClass
	#tag Property, Flags = &h0
		last_synced As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		mobile_timestamp As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_access_method As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_area As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_assignment_number As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_balsa_affected As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_blade_num As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_blade_serial As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_completion_status As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_cover As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_damage_desc As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_damage_severity As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_damage_type As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_end As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_end_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_inspection_damage_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_int_ext As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_length As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_nb_laminate_affected As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_nb_laminate_balsa As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_position_edge As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_position_side As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_priority As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_progress_status As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_radius As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_remark As Text
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_repair_num As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_start_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_time_estimated As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_time_spent As String
	#tag EndProperty

	#tag Property, Flags = &h0
		repairs_wtg_num As String
	#tag EndProperty

	#tag Property, Flags = &h0
		unique_record_identifier As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="last_synced"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mobile_timestamp"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_access_method"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_area"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_assignment_number"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_balsa_affected"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_blade_num"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_blade_serial"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_completion_status"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_cover"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_damage_desc"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_damage_severity"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_damage_type"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_end"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_end_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_inspection_damage_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_int_ext"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_length"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_nb_laminate_affected"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_nb_laminate_balsa"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_position_edge"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_position_side"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_priority"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_progress_status"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_radius"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_remark"
			Group="Behavior"
			Type="Text"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_repair_num"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_start_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_time_estimated"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_time_spent"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="repairs_wtg_num"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="unique_record_identifier"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
