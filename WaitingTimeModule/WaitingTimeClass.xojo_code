#tag Class
Protected Class WaitingTimeClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h0
		Sub Constructor(db As PostgresqlDatabase, company_code As String)
		  Dim strSQL As String = "SELECT last_synced, mobile_timestamp, unique_record_identifier, waitingtime_activity_number, waitingtime_assignment_number, waitingtime_end_date_time," + _
		  " waitingtime_reason, waitingtime_remarks,  waitingtime_start_date_time, waitingtime_team_on_site_date_time, waitingtime_waiting_number" + _
		  " FROM dbglobal.all_waitingtime_with_owners_view" + _
		  " WHERE" + _
		  " ((assignments_supplier_code = $1 AND ((assignments_owner_code = $1) OR (assignments_owner_code <> $1 AND assignments_shared = TRUE)))" + _
		  " OR" + _
		  " ((assignments_operator_code = $1) AND ((assignments_owner_code = $1) OR (assignments_owner_code <> $1 AND assignments_shared = TRUE))))" + _
		  " ORDER BY waitingtime_assignment_number, waitingtime_activity_number, waitingtime_waiting_number;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare( strSQL )
		  pgps.Bind( 0, company_code )
		  
		  If Not db.Error Then
		    waitingtimeRS = pgps.SQLSelect
		  End If
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		last_synced As String
	#tag EndProperty

	#tag Property, Flags = &h0
		mobile_timestamp As String
	#tag EndProperty

	#tag Property, Flags = &h0
		unique_record_identifier As String
	#tag EndProperty

	#tag Property, Flags = &h0
		waitingtimeRS As RecordSet
	#tag EndProperty

	#tag Property, Flags = &h0
		waitingtime_activity_number As String
	#tag EndProperty

	#tag Property, Flags = &h0
		waitingtime_assignment_number As String
	#tag EndProperty

	#tag Property, Flags = &h0
		waitingtime_end_date_time As String
	#tag EndProperty

	#tag Property, Flags = &h0
		waitingtime_reason As String
	#tag EndProperty

	#tag Property, Flags = &h0
		waitingtime_remarks As String
	#tag EndProperty

	#tag Property, Flags = &h0
		waitingtime_start_date_time As String
	#tag EndProperty

	#tag Property, Flags = &h0
		waitingtime_team_on_site_date_time As String
	#tag EndProperty

	#tag Property, Flags = &h0
		waitingtime_waiting_number As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="last_synced"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mobile_timestamp"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="unique_record_identifier"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="waitingtime_activity_number"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="waitingtime_assignment_number"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="waitingtime_end_date_time"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="waitingtime_reason"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="waitingtime_remarks"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="waitingtime_start_date_time"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="waitingtime_team_on_site_date_time"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="waitingtime_waiting_number"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
