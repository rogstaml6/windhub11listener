#tag Module
Protected Module UsersModule
	#tag Method, Flags = &h0
		Function getLoginVerification(dict_param As Xojo.Core.Dictionary) As JSONItem
		  'Dim S As Session = Session
		  
		  mUsers = Nil 
		  mUsers = New UsersClass
		  
		  'Setting language from informaiton sent by mobile app
		  
		  'Setting Default Success response
		  Dim jsonReponse As New JSONItem
		  jsonReponse.Value( "ReturnMessage" ) = App.kSuccess
		  
		  Dim email As String = Lowercase( dict_param.Lookup( "email","" ) )
		  
		  Dim userRS As RecordSet
		  userRS = mUsers.loadDataByFieldIn( App.bdWindhub, "users", "users_email", email, "users_email" )
		  
		  'If  user NOT found
		  If userRS.RecordCount = 0 Then 
		    jsonReponse.Value( "ReturnMessage" ) = UsersModule.NOEMPLOYEDB
		    Return jsonReponse
		  End If
		  
		  'If user found set the parametres for verification and localization update (if necessary)
		  Dim passwordDB As String = userRS.Field( "users_encoded_password" ).StringValue
		  Dim userCompanyCode As String = userRS.Field( "users_company_code" ).StringValue
		  Dim passwordToBeVerified As String = dict_param.Lookup( "password","" )
		  Dim userID As String = userRS.Field( "users_id" ).StringValue
		  Dim dbLanguage As String = userRS.Field( "users_language" ).StringValue
		  
		  userRS.Close
		  userRS = Nil
		  
		  'Set Error Message if the password is wrong
		  If passwordToBeVerified <> passwordDB Then
		    jsonReponse.Value( "ReturnMessage" ) = UsersModule.WRONGPASSWORD
		    Return jsonReponse
		  End If
		  
		  'If the password is right
		  'Set the user language value
		  mUsers.users_language = Lowercase ( dict_param.Lookup( "user_lang", "" ) )
		  
		  'Update users language in the database if different from the language sent by the app 
		  If dbLanguage <> mUsers.users_language Then
		    mUsers.UpdateUserLanguage(  Lowercase( dict_param.Lookup( "user_lang", "" ) ), Val( userID ) ) 
		  End If
		  
		  'Set Response values
		  jsonReponse.Value( "company_code" ) = userCompanyCode
		  jsonReponse.Value( "user_lang" ) = mUsers.users_language
		  jsonReponse.Value( "user_id" ) = userID
		  
		  Return jsonReponse
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getUsersAll(inputDataJson_param As jSonItem) As JSONItem
		  
		  
		  mUsers = Nil 
		  mUsers = New UsersClass
		  
		  Dim userRS As RecordSet
		  Dim user_company_code As String = inputDataJson_param.Lookup( "user_company_code","" )
		  Dim user_id As String = inputDataJson_param.Lookup( "user_id", "" )
		  
		  Dim strSQL As String = "SELECT * FROM dbglobal.users" + _
		  " WHERE" + _
		  " users_company_code = $1" + _
		  " AND users_id = $2;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = App.bdWindhub.Prepare( strSQL )
		  pgps.Bind( 0, user_company_code )
		  pgps.Bind( 1, user_id )
		  
		  userRS = pgps.SQLSelect
		  
		  
		  //userRS = mUsers.loadDataByFieldCond( App.bdWindhub, "users", "users_company_code", " = ", user_company_code, _
		  //" And ", "users_status_id", " = ", "1", "users_id" )
		  
		  If userRS.RecordCount = 0 Then 
		    Dim jsonError As New JSONItem
		    jsonError.Value( "ReturnMessage" ) = UsersModule.NOEMPLOYEDB
		    Return jsonError
		  End If
		  
		  Dim jsonUsers As New JSONItem
		  Dim user As Dictionary
		  
		  While Not userRS.EOF
		    user = New Dictionary
		    user.Value( "users_lastname" ) = userRS.Field( "users_lastname" ).StringValue
		    user.Value( "users_firstname" ) = userRS.Field( "users_firstname" ).StringValue
		    user.Value( "users_address_no" ) = userRS.Field( "users_address_no" ).StringValue
		    user.Value( "users_street" ) = userRS.Field( "users_street" ).StringValue
		    user.Value( "users_city" ) = userRS.Field( "users_city" ).StringValue
		    user.Value( "users_prov_state" ) = userRS.Field( "users_prov_state" ).StringValue
		    user.Value( "users_pc_zip" ) = userRS.Field( "users_pc_zip" ).StringValue
		    user.Value( "users_country" ) = userRS.Field( "users_country" ).StringValue
		    user.Value( "users_tel" ) = userRS.Field( "users_tel" ).StringValue
		    user.Value( "users_email" ) = userRS.Field( "users_email" ).StringValue
		    user.Value( "users_password" ) = userRS.Field( "users_password" ).StringValue
		    user.Value( "users_company_code" ) = userRS.Field( "users_company_code" ).StringValue
		    user.Value( "users_user_idname" ) = userRS.Field( "users_user_idname" ).StringValue
		    user.Value( "users_mobile" ) = userRS.Field( "users_mobile" ).StringValue
		    user.Value( "users_ext" ) = userRS.Field( "users_ext" ).StringValue
		    user.Value( "users_status_id" ) = userRS.Field( "users_status_id" ).StringValue
		    user.Value( "users_language" ) = userRS.Field( "users_language" ).StringValue
		    user.Value( "users_as_technician" ) = userRS.Field( "users_as_technician" ).StringValue
		    jsonUsers.Value( Str( userRS.Field( "users_id" ).IntegerValue ) ) = user
		    userRS.MoveNext
		  Wend
		  
		  userRS.Close
		  userRS = Nil
		  
		  Dim jsonResults As New JSONItem
		  jsonResults.Value( "getUsersAll" ) = jsonUsers
		  jsonResults.Value( "ReturnMessage" ) = App.kSuccess
		  
		  Return jsonResults
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		mUsers As UsersModule.UserSClass
	#tag EndProperty


	#tag Constant, Name = NOEMPLOYEDB, Type = String, Dynamic = True, Default = \"No corresponding employee in the database", Scope = Public
	#tag EndConstant

	#tag Constant, Name = WRONGPASSWORD, Type = String, Dynamic = True, Default = \"Wrong password", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Wrong password"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Erreur de mot de passe"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Wrong password"
		#Tag Instance, Platform = Any, Language = pt-BR, Definition  = \"Usu\xC3\xA1rio ou senha incorreta"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
