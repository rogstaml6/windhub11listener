#tag Class
Protected Class UsersClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h0
		Sub UpdateUserLanguage(new_lang As String, users_id As Integer)
		  
		  Dim strSQL As String = "UPDATE dbglobal.users" + _
		  " SET users_language = $1" + _
		  " WHERE users_id = $2;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = App.bdWindhub.Prepare( strSQL )
		  
		  pgps.Bind( 0, new_lang )
		  pgps.Bind( 1, users_id )
		  
		  pgps.SQLExecute
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		users_address_no As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_city As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_company_code As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_country As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_email As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_ext As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_firstname As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		users_language As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_lastname As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_mobile As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_password As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_pc_zip As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_prov_state As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_status_id As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_street As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_tel As String
	#tag EndProperty

	#tag Property, Flags = &h0
		users_user_idname As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_address_no"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_city"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_company_code"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_country"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_email"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_ext"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_firstname"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_language"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_lastname"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_mobile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_password"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_pc_zip"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_prov_state"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_status_id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_street"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_tel"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="users_user_idname"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
