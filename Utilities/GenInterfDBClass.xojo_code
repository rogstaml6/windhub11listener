#tag Class
Protected Class GenInterfDBClass
	#tag Method, Flags = &h0
		Function extract_prefix(field As String) As String
		  
		  // Extraire le préfix du champ 
		  
		  Dim fieldTable(-1) As String = Split(field, "_")
		  
		  Return fieldTable(0)
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function loadData(dB As PostgreSQLDatabase, table_name As String, orderBy1 as String, Optional orderBy2 as String, Optional orderBy3 as String, Optional orderBy4 as String) As RecordSet
		  Dim genRS As RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  strSQL = "SELECT * FROM " + table_Name + " Order By  " + orderBy1
		  
		  If orderBy2 <> "" then strSQL = strSQL + " ," + orderBy2
		  If orderBy3 <> "" then strSQL = strSQL + " ," + orderBy3
		  If orderBy4 <> "" then strSQL = strSQL + " ," + orderBy4
		  
		  genRS =  dB.SQLSelect(strSQL)
		  compteur = genRS.RecordCount
		  
		  return genRS
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function loadDataByFieldCond(dB As PostgreSQLDatabase , table_name As String, field1 As String, operateur1 As String, value1 As String, connecteur As String, field2 As String, operateur2 As String, value2 As String, orderBy1 as String, Optional orderBy2 as String, Optional orderBy3 as String, Optional orderBy4 as String) As RecordSet
		  Dim genRS As RecordSet
		  Dim strSQL As  String = ""
		  Dim clauseWhere As  String = ""
		  Dim compteur As Integer
		  Dim pgps As PostgreSQLPreparedStatement
		  
		  Select Case field2
		  Case Is =  ""
		    
		    clauseWhere = " WHERE  " + field1  + operateur1 + "$1 "
		    
		    strSQL = "SELECT * FROM " + table_Name + clauseWhere + " ORDER BY  " + orderBy1
		    
		    If orderBy2 <> "" Then strSQL = strSQL + " ," + orderBy2
		    If orderBy3 <> "" Then strSQL = strSQL + " ," + orderBy3
		    If orderBy4 <> "" Then strSQL = strSQL + " ," + orderBy4
		    
		    pgps = dB.Prepare(strSQL)
		    pgps.Bind(0, value1)
		    
		  Case Is <> ""
		    
		    clauseWhere = " WHERE  " + field1  + operateur1 + "$1 "
		    clauseWhere = clauseWhere + connecteur + field2  + operateur2 + "$2 "
		    
		    
		    strSQL = "SELECT * FROM " + table_Name + clauseWhere + " ORDER BY  " + orderBy1
		    
		    
		    If orderBy2 <> "" Then strSQL = strSQL + " ," + orderBy2
		    If orderBy3 <> "" Then strSQL = strSQL + " ," + orderBy3
		    If orderBy4 <> "" Then strSQL = strSQL + " ," + orderBy4
		    
		    pgps = dB.Prepare(strSQL)
		    pgps.Bind(0, value1)
		    pgps.Bind(1, value2)
		    
		  End Select
		  
		  
		  genRS =  pgps.SQLSelect
		  compteur = genRS.RecordCount
		  
		  Return genRS
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function loadDataByFieldIn(dB As PostgreSQLDatabase, table_name As String, field As String, value As String, orderBy as String) As RecordSet
		  Dim genRS As RecordSet
		  Dim strSQL As String = ""
		  Dim compteur As Integer
		  
		  strSQL = "SELECT * FROM " + table_Name + " WHERE  " + field  + " IN($1) ORDER BY  " + orderBy
		  
		  Dim pgps As PostgreSQLPreparedStatement = dB.Prepare(strSQL)
		  pgps.Bind(0, value)
		  
		  genRS =  pgps.SQLSelect
		  compteur = genRS.RecordCount
		  
		  Return genRS
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub loadPropertyDict(prefix_param As String)
		  // Dictionnaire des propriétés de la classe
		  
		  Dim propDict As New Dictionary
		  Dim ti as Introspection.typeinfo = Introspection.GetType(Self)
		  
		  For each prop As Introspection.PropertyInfo in ti.GetProperties()
		    If prop.IsPublic and prop.CanRead and prop.CanWrite Then
		      Dim propName As String = prop.Name
		      Dim extractionPrefix As String = Self.extract_prefix(propName)
		      If extractionPrefix <> prefix_param then Continue
		      
		      propDict.Value(prop.Name) = prop
		      Select Case prop.PropertyType.FullName
		        
		      Case "Boolean"
		        prop.Value(Self)  = False
		      Case "Currency"
		        prop.Value(Self)  = 0
		      Case "Date"
		        prop.Value(Self)  = new Date
		      Case "Double"
		        prop.Value(Self)  = 0
		      Case "Integer"
		        prop.Value(Self)  = 0
		      Case "Int32"
		        prop.Value(Self)  = 0
		      Case "Int64"
		        prop.Value(Self)  = 0
		        'Case "Picture"
		        'prop.Value(Self)  = rS.Field(prop.name).PictureValue
		      Case "String"
		        prop.Value(Self)  = ""
		      Case Else
		        prop.Value(Self)  = ""
		      End Select
		      
		    End If
		  Next
		  
		  If Not(Self.propertyDict Is Nil) Then Self.propertyDict = Nil
		  Self.propertyDict = propDict
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub writeDataBD(rS As RecordSet, repairDic As Dictionary)
		  
		  For Each key As String In Self.propertyDict.Keys
		    Dim prop As Introspection.PropertyInfo = Self.propertyDict.Value(key)
		    Dim propName As String = prop.Name
		    
		    Dim variantProp As Variant = repairDic.Lookup(propName,"")
		    
		    Select Case prop.PropertyType.FullName
		      
		    Case "Boolean"
		      rS.Field(key).BooleanValue = variantProp.BooleanValue
		    Case "Currency"
		      rS.Field(key).CurrencyValue = variantProp.CurrencyValue
		    Case "Date"
		      rS.Field(key).DateValue = variantProp.DateValue
		    Case "Double"
		      rS.Field(key).DoubleValue = variantProp.DoubleValue
		    Case "Integer"
		      rS.Field(key).IntegerValue = variantProp.IntegerValue
		    Case "Int32"
		      rS.Field(key).IntegerValue = variantProp.IntegerValue
		    Case "Int64"
		      rS.Field(key).Int64Value = variantProp.Int64Value
		    Case "Picture"
		      rS.Field(key).PictureValue = variantProp
		    Case "String"
		      If rS.Field(key) = Nil Then
		        rS.Field(key).StringValue  = ""
		      Else
		        rS.Field(key).StringValue = variantProp.StringValue
		      End If
		    End Select
		    
		  Next
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		propertyDict As Dictionary
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
