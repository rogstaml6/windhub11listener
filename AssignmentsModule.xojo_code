#tag Module
Protected Module AssignmentsModule
	#tag Method, Flags = &h0
		Function getAssignmentData(inputDataJson_param As jSonItem, assignmentDataDict_param As Dictionary) As Dictionary
		  
		  
		  Dim dictResults As New Dictionary
		  Dim user_company_code As String = inputDataJson_param.Lookup( "user_company_code","" )
		  mUserID = inputDataJson_param.Lookup( "user_id","" ).IntegerValue
		  
		  mAssignments = Nil 
		  mAssignments = New AssignmentsModule.AssignmentsClass( App.bdWindhub, user_company_code, mUserID )
		  Dim aRS As Recordset = mAssignments.assignmentsRS
		  
		  Dim userRS As RecordSet
		  userRS = mAssignments.loadDataByFieldIn( App.bdWindhub, "dbglobal.all_companies_view", "company_code", user_company_code, "company_name" )
		  
		  If userRS.RecordCount = 0 Then 
		    dictResults.Value( "ReturnMessage" ) = App.kSuccess
		    Return dictResults
		  End If
		  
		  mCompanyRole = userRS.Field( "company_role" ).StringValue
		  userRS.Close
		  userRS = Nil
		  
		  
		  //Dim aRS As RecordSet
		  //If userCompanyRole = "Operator" Then
		  //aRS = mAssignments.loadDataByFieldIn( App.bdWindhub, "assignments", "assignments_operator_code", _
		  //user_company_code, "assignments_number" )
		  //Else
		  //aRS = mAssignments.loadDataByFieldIn( App.bdWindhub, "assignments", "assignments_supplier_code", _
		  //user_company_code, "assignments_number" )
		  //End If
		  
		  
		  
		  If aRS.RecordCount = 0 Then 
		    dictResults.Value( "ReturnMessage" ) = App.kSuccess
		    Return dictResults
		  End If
		  
		  Dim dictAssignments As New Dictionary
		  Dim assignments As Dictionary
		  
		  While Not aRS.EOF
		    assignments = New Dictionary
		    Dim dictLookup As Dictionary = assignmentDataDict_param.Lookup( "assignments","" ) 
		    For i As Integer = 0 To dictLookup.Count - 1
		      
		      Dim fieldName As String = dictLookup.Key( i ).StringValue
		      
		      Dim propertyField As Dictionary = dictLookup.Lookup( fieldName,"" )
		      
		      Dim dataType As String = propertyField.Lookup( "datatype","" )
		      
		      Select Case dataType
		        
		      Case "boolean"
		        assignments.Value( fieldName ) = Str( aRS.Field( fieldName ).BooleanValue )
		      Case "date"
		        If aRS.Field( fieldName ).DateValue <> Nil Then
		          assignments.Value( fieldName ) = aRS.Field( fieldName ).StringValue
		        End If
		      Case "double"
		        assignments.Value( fieldName ) = Str( aRS.Field( fieldName ).DoubleValue ) 
		      Case "integer"
		        assignments.Value( fieldName ) = Str( aRS.Field( fieldName ).IntegerValue )
		      Case "smallint"
		        assignments.Value( fieldName ) = Str( aRS.Field( fieldName ).IntegerValue )
		      Case "bigint"
		        assignments.Value( fieldName ) = Str( aRS.Field( fieldName ).Int64Value )
		      Case "character varying"
		        assignments.Value( fieldName ) = aRS.Field( fieldName ).StringValue
		      Case "text"
		        assignments.Value( fieldName ) = aRS.Field( fieldName ).StringValue
		      Case "numeric"
		        assignments.Value( fieldName ) = Str( aRS.Field( fieldName ).DoubleValue )
		      Case "uuid"
		        assignments.Value( fieldName ) = aRS.Field( fieldName ).StringValue
		      Case "timestamp without time zone"
		        assignments.Value( fieldName ) = aRS.Field( fieldName ).StringValue
		      Case "timestamp with time zone"
		        assignments.Value( fieldName ) = aRS.Field( fieldName ).StringValue
		      Case "point"
		        assignments.Value( fieldName ) = aRS.Field( fieldName ).StringValue
		      Case "double precision"
		        assignments.Value( fieldName ) = Str( aRS.Field( fieldName ).DoubleValue ) 
		      End Select
		      
		      
		      
		    Next
		    //Exceptions
		    assignments.Value( "user_id" ) =  aRS.Field( "assignments_users_user_id" ).StringValue
		    assignments.Value( "assignments_total_waiting_duration" ) = aRs.Field(  "total_waitingtime" ).StringValue
		    assignments.Value( "assignments_activity_number" ) = aRs.Field(  "activity_number" ).StringValue
		    assignments.Value( "activity_uuid" ) = aRs.Field(  "activity_uuid" ).StringValue
		    
		    dictAssignments.Value( Str( aRS.Field( "assignments_number" ).StringValue ) + " " + Str( aRS.Field( "activity_number" ).IntegerValue, "000" ) ) = assignments
		    aRS.MoveNext
		  Wend
		  
		  aRS.Close
		  aRS = Nil
		  
		  dictResults.Value( "getAssignmentData" ) = dictAssignments
		  dictResults.Value( "ReturnMessage" ) = App.kSuccess
		  
		  Return dictResults
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		mAssignments As AssignmentsModule.AssignmentsClass
	#tag EndProperty


	#tag Constant, Name = NOASSIGNMENT, Type = String, Dynamic = True, Default = \"Aucune assignation dans la base de donn\xC3\xA9es", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No assignment in the database"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Aucune assignation dans la base de donn\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No assignment in the database"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
