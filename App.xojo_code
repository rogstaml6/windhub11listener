#tag Class
Protected Class App
Inherits WebApplication
	#tag Event
		Function HandleSpecialURL(Request As WebRequest) As Boolean
		  Dim message As String = App.kSuccess
		  Dim jsonReturn As New JSONItem
		  
		  // Connection to database
		  DB_Connection
		  
		  //Request reception
		  Select Case Request.Method
		  Case "POST"
		    Select Case Request.Path
		      
		    Case "getLoginVerification"
		      message = connectDB
		      If message <> App.kSuccess.ToText Then
		        '' Message à faire parvenir à l'administrateur
		        //SendMail( message )
		        jsonReturn.Value( "ReturnMessage" ) = message
		        Request.Print( jsonReturn.ToString )
		        disconnectDB
		        Return True
		      End If
		      
		      Dim jsonData As Text 
		      #If DebugBuild Then
		        jsonData = "{""email"":""foutech11@gmail.com"",""password"":""031F601EC089A4E57D4E4F55C5DCE0C93F2D0BE157D727E244C7FF2FE4918005"",""user_lang"":""en""}"
		        //jsonData = "{""email"":""yann.proust@mistrasgroup.com"",""password"":""F4357E28F15020FFA14D6389FDEB0179D1FB3D66FCB85F99ED2DAB6D5519E603"",""user_lang"":""en""}"
		        
		      #Else
		        jsonData = DefineEncoding( Request.Entity, Encodings.UTF8 ).ToText
		      #EndIf
		      
		      Dim jsonDict As Xojo.Core.Dictionary
		      jsonDict = Xojo.Data.ParseJSON( jsonData )
		      
		      // Lecture des infos
		      jsonReturn = UsersModule.getLoginVerification( jsonDict )
		      message = jsonReturn.Value( "ReturnMessage" ).StringValue.ToText
		      If message <> App.kSuccess.ToText Then
		        // Message à faire parvenir à l'administrateur
		        SendMail( message + App.bdWindhub.Host )
		      End If
		      
		      // Send back data
		      Request.Print( jsonReturn.ToString )
		      disconnectDB
		      Return True
		      
		    Case "sendInspectionUpdate"
		      //Dim lang As String = Request.GetParameter( "lang" )
		      message = connectDB
		      If message <> App.kSuccess.ToText Then
		        // Message à faire parvenir à l'administrateur
		        SendMail( message )
		        jsonReturn.Value( "ReturnMessage" ) = message
		        Request.Print( jsonReturn.ToString.ToText )
		        disconnectDB
		        Return True
		      End If
		      
		      //Dim jsonStr As String
		      
		      //#If DebugBuild
		      //Dim f As FolderItem
		      //f = GetFolderItem( "" ).Parent.Child( "sendInspectionUpdate.json" )
		      //If f.Exists Then
		      //Dim jsonStream As TextInputStream
		      //Try
		      //jsonStream = TextInputStream.Open( f )
		      //jsonStream.Encoding = Encodings.UTF8
		      //jsonStr = jsonStream.ReadAll
		      //Catch e As IOException
		      //jsonStream.Close
		      //MsgBox( "ERROR" )
		      //End Try
		      //End If
		      //#EndIf
		      
		      //Dim jsonData As Text = jsonstr.ToText
		      
		      Dim jsonData As Text = DefineEncoding( Request.Entity, Encodings.UTF8 ).ToText
		      Dim activeAssignDic, inspectionsUpdateDic, inspectionsDic, damagesDic, waitingtimesDic As Xojo.Core.Dictionary
		      Dim mobileUUID, rUpdatedInspections(), rUpdatedDamages(), rUpdatedWaitingTime() As String
		      
		      //Lecture des INSPECTIONS
		      Try
		        If jsonData <> "" Then
		          inspectionsUpdateDic = Xojo.Data.ParseJSON( jsonData )
		        End If
		      Catch e As Xojo.Data.InvalidJSONException
		        jsonReturn.Value( "ReturnMessage" ) = "Invalid JSON string"
		        Request.Print( jsonReturn.ToString )
		        disconnectDB
		        Return True
		      End Try
		      
		      
		      //Classer les inspections dans un dictionnaire
		      If inspectionsUpdateDic <> Nil And inspectionsUpdateDic.HasKey( "inspections" ) Then
		        inspectionsDic = inspectionsUpdateDic.Value( "inspections" )
		        
		        //Mise à jour des inspections et création du dictionnaire des assignations actives
		        activeAssignDic = New Xojo.Core.Dictionary 'Dictionnaire des assignations actives
		        Dim foundCompletedEntries As Integer = 0 'Pour éviter l'envoi d'une erreur si tous les projets sont marquées "Complétées"
		        
		        For Each entry As Xojo.Core.DictionaryEntry In inspectionsDic
		          'Déterminer si le l'assignation est complétée. Dans ce cas, on ne le met pas à jour et on ne l'ajoute pas au dictionnaire des projets actifs
		          Dim assignNum As String = Xojo.Core.Dictionary( entry.Value ).Value( "inspections_assignment_number" )
		          Dim inspUniqueID As String = Xojo.Core.Dictionary( entry.Value ).Value( "unique_record_identifier" )
		          //Dim OwnerOrShared As Boolean = Ownership_And_Sharing( assignNum ) ' Checks if the project sent is owned( created ) by the users company or is still shared by the the creator
		          ' Until the mobile App is modified
		          Dim OwnerOrShared As Boolean = True
		          
		          Dim assignStatusID As String = Get_Assignment_Status( assignNum )
		          
		          If assignStatusID <> "4" And OwnerOrShared Then ' 4 = "Complété
		            
		            If activeAssignDic.HasKey( assignNum ) Then
		              Dim tempArr() As String = activeAssignDic.Value( assignNum )
		              tempArr.Append( inspUniqueID )
		              activeAssignDic.Value( assignNum ) = tempArr
		            Else
		              Dim inspArray() As String
		              inspArray.Append( inspUniqueID )
		              activeAssignDic.Value( assignNum ) = inspArray ' Key = assignNum and Value = Array( Inspection Unique record Identifier )
		            End If
		            
		            //Mettre à jour
		            message = UpdateInspection( entry.Value )
		            
		            // Ajout de l'identifiant unique de l'entrée mise à jour
		            If message = App.kSuccess.ToText Then
		              rUpdatedInspections.Append( inspUniqueID )
		            Else
		              Continue
		            End If
		            
		          Else
		            foundCompletedEntries = foundCompletedEntries + 1
		          End If
		          
		        Next entry
		        
		        If message <> App.kSuccess.ToText  And foundCompletedEntries <> activeAssignDic.Count Then
		          // Message à faire parvenir à l'administrateur
		          SendMail( "Problem with inspection update" )
		          jsonReturn.Value( "ReturnMessage" ) = message
		          Request.Print( jsonReturn.ToString )
		          disconnectDB
		          Return True
		        End If
		      Else
		        message = App.kSuccess.ToText
		      End If
		      
		      //Update des DOMMAGES
		      If inspectionsUpdateDic <> Nil And inspectionsUpdateDic.HasKey( "inspectiondamages" ) Then
		        damagesDic = inspectionsUpdateDic.Value( "inspectiondamages" ) //Dictionnaire des dommages envoyés Key = "num" and Value = dictionary of one damage 
		        If damagesDic.Count > 0 Then
		          
		          //Mise à jour des inspections et création du dictionnaire des affectations actives
		          activeAssignDic = New Xojo.Core.Dictionary 'Dictionnaire des affectations actives
		          
		          For Each entry As Xojo.Core.DictionaryEntry In damagesDic
		            Dim assignNum As String = Xojo.Core.Dictionary( entry.Value ).Value( "inspectiondamages_assignment_number" )
		            Dim damDetetedFlag As String = Xojo.Core.Dictionary( entry.Value ).Value( "is_deleted" )
		            Dim damUniqueID As String = Xojo.Core.Dictionary( entry.Value ).Value( "unique_record_identifier" )
		            Dim damUpdatable As Boolean = Get_Damage_Updatable( damUniqueID )
		            //Dim OwnerOrShared As Boolean = Ownership_And_Sharing( projNum ) ' Checks if the project sent is owned( created ) by the users company or is still shared by the the creator
		            ' Until the mobile App is modified
		            Dim OwnerOrShared As Boolean = True
		            
		            //Dim assignStatusID As String = Get_Project_Status( projNum )
		            Dim assignStatusID As String = Get_Assignment_Status( assignNum )
		            If assignStatusID <> "4" And OwnerOrShared Then ' 4 = "Complété
		              
		              If activeAssignDic.HasKey( assignNum ) Then
		                Dim tempArr() As String = activeAssignDic.Value( assignNum )
		                tempArr.Append( damUniqueID )
		                activeAssignDic.Value( assignNum ) = tempArr
		              Else
		                Dim inspArray() As String
		                inspArray.Append( damUniqueID )
		                activeAssignDic.Value( assignNum ) = inspArray ' Key = projNum and Value = Array( Inspection Unique record Identifier )
		              End If
		              
		              //Effacement du dommage et des photos
		              If damUpdatable And activeAssignDic.HasKey( assignNum ) And damDetetedFlag = "1" Then  ' 0 = not deleted and 1 = deleted
		                message = Delete_Damages( assignNum, damUniqueID )
		                If message = App.kSuccess.ToText Then
		                  rUpdatedDamages.Append( damUniqueID )
		                Else
		                  ' Message à faire parvenir à l'administrateur
		                  SendMail( "Problem with deleting damages" )
		                  jsonReturn.Value( "ReturnMessage" ) = message
		                  Request.Print( jsonReturn.ToString )
		                  disconnectDB
		                  Return True
		                End If
		              End If
		              
		              //Mise à jour du dommage et des photos
		              If damUpdatable And activeAssignDic.HasKey( assignNum ) And damDetetedFlag = "0" Then ' 0 = not deleted and 1 = deleted
		                
		                'Déterminer si on UPDATE ou INSERT
		                If damageExists( damUniqueID ) Then
		                  message = UpdateInspectionDamage( Xojo.Core.Dictionary( entry.Value ) )
		                Else
		                  message = AddInspectionDamage( Xojo.Core.Dictionary( entry.Value ) )
		                End If
		                
		                'Ajouter aux dommages modifiés
		                If message = App.kSuccess.ToText Then
		                  rUpdatedDamages.Append( damUniqueID )
		                Else
		                  // Message à faire parvenir à l'administrateur
		                  SendMail( "Problem With damages update" )
		                  jsonReturn.Value( "ReturnMessage" ) = message
		                  Request.Print( jsonReturn.ToString )
		                  disconnectDB
		                  Return True
		                End If
		              End If
		            End If
		          Next Entry
		          
		        End If
		      Else
		        message = App.kSuccess.ToText
		      End If
		      
		      //Update du WaitingTime
		      If inspectionsUpdateDic <> Nil And inspectionsUpdateDic.HasKey( "waitingtimes" ) Then
		        waitingtimesDic = inspectionsUpdateDic.Value( "waitingtimes" ) //Dictionnaire des dommages envoyés Key = "num" and Value = dictionary of one damage 
		        
		        If waitingtimesDic.Count > 0 Then
		          
		          //Mise à jour des waitingtimes et création du dictionnaire des affectations actives
		          activeAssignDic = New Xojo.Core.Dictionary 'Dictionnaire des affectations actives
		          
		          For Each entry As Xojo.Core.DictionaryEntry In waitingtimesDic
		            Dim assignNum As String = Xojo.Core.Dictionary( entry.Value ).Value( "waitingtime_assignment_number" )
		            Dim waitDetetedFlag As String = Xojo.Core.Dictionary( entry.Value ).Value( "is_deleted" )  '*Not needed for now
		            Dim waitUniqueID As String = Xojo.Core.Dictionary( entry.Value ).Value( "unique_record_identifier" )
		            'Dim waitUpdatable As Boolean = Get_xxx_Updatable( damUniqueID ) * Not needed for now
		            //Dim OwnerOrShared As Boolean = Ownership_And_Sharing( projNum ) ' Checks if the project sent is owned( created ) by the users company or is still shared by the the creator
		            ' Until the mobile App is modified
		            Dim OwnerOrShared As Boolean = True
		            
		            Dim assignStatusID As String = Get_Assignment_Status( assignNum )
		            If assignStatusID <> "4" And OwnerOrShared Then ' 4 = "Complété
		              
		              If activeAssignDic.HasKey( assignNum ) Then
		                Dim tempArr() As String = activeAssignDic.Value( assignNum )
		                tempArr.Append( waitUniqueID )
		                activeAssignDic.Value( assignNum ) = tempArr
		              Else
		                Dim inspArray() As String
		                inspArray.Append( waitUniqueID )
		                activeAssignDic.Value( assignNum ) = inspArray ' Key = assignNum and Value = Array( Inspection Unique record Identifier )
		              End If
		              
		              '//Effacement du WaitingTime (Pas utilisé pour l'instant)
		              If  activeAssignDic.HasKey( assignNum ) And waitDetetedFlag = "1" Then  ' 0 = not deleted and 1 = deleted
		                message = Delete_Waitingtime( assignNum, waitUniqueID )
		                If message = App.kSuccess.ToText Then
		                  rUpdatedWaitingTime.Append( waitUniqueID )
		                Else
		                  '' Message à faire parvenir à l'administrateur
		                  SendMail( "Problem with deleting waitingtime" )
		                  jsonReturn.Value( "ReturnMessage" ) = message
		                  Request.Print( jsonReturn.ToString )
		                  disconnectDB
		                  Return True
		                End If
		              End If
		              
		              //Mise à jour du waitingtime
		              'If damUpdatable And activeAssignDic.HasKey( assignNum ) And damDetetedFlag = "0" Then ' 0 = not deleted and 1 = deleted
		              If activeAssignDic.HasKey( assignNum ) And waitDetetedFlag = "0" Then  ' 0 = not deleted and 1 = deletedThen ' 0 = not deleted and 1 = deleted
		                'Déterminer si on UPDATE ou INSERT
		                If WaitingtimeExists( waitUniqueID ) Then
		                  message = UpdateWaitingtime( Xojo.Core.Dictionary( entry.Value ) )
		                Else
		                  message = AddWaitingtime( Xojo.Core.Dictionary( entry.Value ) )
		                End If
		                
		                'Ajouter aux waitingtime modifiés
		                If message = App.kSuccess.ToText Then
		                  rUpdatedWaitingTime.Append( waitUniqueID )
		                Else
		                  // Message à faire parvenir à l'administrateur
		                  SendMail( "Problem With damages update" )
		                  jsonReturn.Value( "ReturnMessage" ) = message
		                  Request.Print( jsonReturn.ToString )
		                  disconnectDB
		                  Return True
		                End If
		              End If
		            End If
		          Next Entry
		          
		        End If
		        
		      Else
		        message = App.kSuccess
		      End If
		      
		      
		      
		      // Send back response with return data selon ce modèle
		      
		      ' {
		      ' "ReturnMessage": App.kSuccess.ToText,
		      ' "data": {
		      ' "inspections": ["a460693b-7f4d-4953-8fe3-d9cd06458e0a", "93b1ee01-b863-4145-a13e-8910034edcbe", "59562cc5-7369-476c-a5a1-a43e7cd50e9c"],
		      ' "inspectiondamages": ["59562cc5-7369-476c-a5a1-a43e7cd50e9c", "a460693b-7f4d-4953-8fe3-d9cd06458e0a", "93b1ee01-b863-4145-a13e-8910034edcbe"]
		      ' }
		      ' }
		      Dim returnDic As New Xojo.Core.Dictionary
		      returnDic.Value( "ReturnMessage" ) = Message
		      
		      'valeur de l'item data qui contient trois array soient celui des identifiants uniques des inspections mises à jour et celui des dentifiants uniques des dommages mis à jour et ceux des dommages effacés
		      Dim rDataDic As New Xojo.Core.Dictionary
		      rDataDic.Value( "inspections" ) =  rUpdatedInspections
		      rDataDic.Value( "inspectiondamages" ) =  rUpdatedDamages
		      rDataDic.Value( "waitingtimes" ) = rUpdatedWaitingTime
		      
		      returnDic.Value( "data" ) = Xojo.Data.GenerateJSON( rDataDic )
		      Request.Print( Xojo.Data.GenerateJSON( returnDic ) )
		      
		      disconnectDB
		      Return True
		      
		      
		      
		    Case "sendRepairUpdate"
		      
		      message = connectDB
		      If message <> App.kSuccess.ToText Then
		        // Message à faire parvenir à l'administrateur
		        //SendMail( message )
		        jsonReturn.Value( "ReturnMessage" ) = message
		        Request.Print( jsonReturn.ToString )
		        disconnectDB
		        Return True
		      End If
		      
		      //Dim jsonStr As String
		      //
		      //#If DebugBuild
		      //Dim f As FolderItem
		      //f = GetFolderItem( "" ).Parent.Child( "sendRepairUpdate2.json" )
		      //If f.Exists Then
		      //Dim jsonStream As TextInputStream
		      //Try
		      //jsonStream = TextInputStream.Open( f )
		      //jsonStream.Encoding = Encodings.UTF8
		      //jsonStr = jsonStream.ReadAll
		      //Catch e As IOException
		      //jsonStream.Close
		      //MsgBox( "ERROR" )
		      //End Try
		      //End If
		      //#EndIf
		      
		      //Dim jsonData As Text = jsonstr.ToText
		      
		      
		      Dim jsonData As Text = DefineEncoding( Request.Entity, Encodings.UTF8 ).ToText
		      Dim activeAssignDic, repairsUpdateDic, repairsDic, stepsDic, waitingtimesDic As Xojo.Core.Dictionary
		      Dim mobileUUID, rUpdatedRepairs(), rUpdatedSteps(), rUpdatedWaitingTime() As String
		      
		      
		      //Lecture des Réparations
		      Try
		        If jsonData <> "" Then
		          repairsUpdateDic = Xojo.Data.ParseJSON( jsonData )
		        End If
		      Catch e As Xojo.Data.InvalidJSONException
		        jsonReturn.Value( "ReturnMessage" ) = "Invalid JSON string"
		        Request.Print( jsonReturn.ToString )
		        disconnectDB
		        Return True
		      End Try
		      
		      //Classer les réparations dans un dictionnaire
		      If repairsUpdateDic <> Nil And repairsUpdateDic.HasKey( "repairs" ) Then
		        repairsDic = repairsUpdateDic.Value( "repairs" )
		        
		        //Update des réparations et création du dictionnaire des affectations actives
		        activeAssignDic = New Xojo.Core.Dictionary 'Dictionnaire des affectations actives
		        
		        Dim assignNum As String
		        Dim foundCompletedEntries As Integer = 0 'Pour éviter l'envoi d'une erreur si tous les projets sont marquées "Complétées"
		        
		        For Each entry As Xojo.Core.DictionaryEntry In repairsDic
		          
		          //Déterminer si le projet est complété. Dans ce cas, on ne le met pas à jour et on ne l'ajoute pas au dictionnaire des projets actifs
		          assignNum = Xojo.Core.Dictionary( entry.Value ).Value( "repairs_assignment_number" )
		          Dim repUniqueID As String = Xojo.Core.Dictionary( entry.Value ).Value( "unique_record_identifier" )
		          //Dim OwnerOrShared As Boolean = Ownership_And_Sharing( assignNum ) ' Checks if the project sent is owned( created ) by the users company or is still shared by the the creator
		          ' Until the mobile App is modified
		          Dim OwnerOrShared As Boolean = True
		          
		          Dim assignStatusID As String = Get_Assignment_Status( assignNum )
		          
		          If assignStatusID <> "4" And OwnerOrShared Then // 4 = "Complété
		            
		            If activeAssignDic.HasKey( assignNum ) Then
		              Dim tempArr() As String = activeAssignDic.Value( assignNum )
		              tempArr.Append( repUniqueID )
		              activeAssignDic.Value( assignNum ) = tempArr
		            Else
		              Dim repNumArray() As String
		              repNumArray.Append( repUniqueID )
		              activeAssignDic.Value( assignNum ) = repNumArray  'Key = assignNum and Value = Array( Inspection Unique record Identifier )
		            End If
		            
		            //Mettre à jour
		            message = UpdateRepair( entry.Value )
		            
		            // Ajout de l'identifiant unique de l'entrée mise à jour
		            If message = App.kSuccess.ToText Then
		              rUpdatedRepairs.Append( repUniqueID )
		            Else
		              Continue
		            End If
		            
		          Else
		            foundCompletedEntries = foundCompletedEntries + 1
		          End  If
		          
		        Next entry
		        
		        If message <> App.kSuccess.ToText And foundCompletedEntries <> activeAssignDic.Count Then
		          // Message à faire parvenir à l'administrateur
		          SendMail( "Problem With repair update" )
		          jsonReturn.Value( "ReturnMessage" ) = message
		          Request.Print( jsonReturn.ToString )
		          disconnectDB
		          Return True
		        End If
		        
		      Else
		        message = App.kSuccess
		      End If
		      
		      
		      //Update des Étapes
		      If repairsUpdateDic <> Nil And repairsUpdateDic.HasKey( "repairsteps" ) Then
		        stepsDic = repairsUpdateDic.Value( "repairsteps" ) //Dictionnaire des étapes envoyés Key = "num" and Value = dictionary of one step 
		        If stepsDic.Count > 0 Then
		          
		          //Update des réparations et création du dictionnaire des projets actifs
		          activeAssignDic = New Xojo.Core.Dictionary 'Dictionnaire des projets actifs
		          
		          For Each entry As Xojo.Core.DictionaryEntry In stepsDic
		            //Déterminer si le projet est complété. Dans ce cas, on ne le met pas à jour et on ne l'ajoute pas au dictionnaire des projets actifs
		            Dim assignNum As String = Xojo.Core.Dictionary( entry.Value ).Value( "repairsteps_assignment_number" )
		            Dim stepDeletedFlag As String = Xojo.Core.Dictionary( entry.Value ).Value( "is_deleted" )
		            Dim stepUniqueID As String = Xojo.Core.Dictionary( entry.Value ).Value( "unique_record_identifier" )
		            //Dim OwnerOrShared As Boolean = Ownership_And_Sharing( assignNum ) ' Checks if the project sent is owned( created ) by the users company or is still shared by the the creator
		            ' Until the mobile App is modified
		            Dim OwnerOrShared As Boolean = True
		            
		            Dim assignStatusID As String = Get_Assignment_Status( assignNum )
		            
		            If assignStatusID <> "4" And OwnerOrShared Then // 4 = "Complété
		              
		              If activeAssignDic.HasKey( assignNum ) Then
		                Dim tempArr() As String = activeAssignDic.Value( assignNum )
		                tempArr.Append( stepUniqueID )
		                activeAssignDic.Value( assignNum ) = tempArr
		              Else
		                Dim repNumArray() As String
		                repNumArray.Append( stepUniqueID )
		                activeAssignDic.Value( assignNum ) = repNumArray  'Key = assignNum and Value = Array( Inspection Unique record Identifier )
		              End If
		              
		              
		              //Effacement des étapes et des photos
		              If  activeAssignDic.HasKey( assignNum ) And stepDeletedFlag = "1" Then  '0 = Not deleted And 1 = deleted
		                message = Delete_Steps( assignNum, stepUniqueID )
		                If message = App.kSuccess.ToText Then
		                  rUpdatedSteps.Append( stepUniqueID )
		                Else
		                  // Message à faire parvenir à l'administrateur
		                  SendMail( "Problem With deleting steps" )
		                  jsonReturn.Value( "ReturnMessage" ) = "Problem With deleting steps"
		                  Request.Print( jsonReturn.ToString )
		                  disconnectDB
		                  Return True
		                End If
		              End If
		              
		              //Mise à jour de l'étape et des photos
		              If  activeAssignDic.HasKey( assignNum ) And stepDeletedFlag = "0" Then ' 0 = Not deleted And 1 = deleted
		                
		                'Déterminer si on UPDATE ou INSERT
		                If stepExists( stepUniqueID ) Then
		                  message = UpdateRepairStep( Xojo.Core.Dictionary( entry.Value ) )
		                Else
		                  message = AddRepairStep( Xojo.Core.Dictionary( entry.Value ) )
		                End If
		                
		                'Ajouter aux étapes modifiées
		                If message = App.kSuccess.ToText Then
		                  rUpdatedSteps.Append( stepUniqueID )
		                Else
		                  // Message à faire parvenir à l'administrateur
		                  SendMail( "Problem With steps update" )
		                  jsonReturn.Value( "ReturnMessage" ) = "Problem With steps update"
		                  Request.Print( jsonReturn.ToString )
		                  disconnectDB
		                  Return True
		                End If
		              End If
		            End If
		          Next entry
		        End If
		      Else
		        message = App.kSuccess
		      End If
		      
		      //Update du WaitingTime
		      If repairsUpdateDic <> Nil And repairsUpdateDic.HasKey( "waitingtimes" ) Then
		        waitingtimesDic = repairsUpdateDic.Value( "waitingtimes" ) //Dictionnaire des dommages envoyés Key = "num" and Value = dictionary of one damage 
		        
		        If waitingtimesDic.Count > 0 Then
		          
		          //Mise à jour des waitingtimes et création du dictionnaire des affectations actives
		          activeAssignDic = New Xojo.Core.Dictionary 'Dictionnaire des affectations actives
		          
		          For Each entry As Xojo.Core.DictionaryEntry In waitingtimesDic
		            Dim assignNum As String = Xojo.Core.Dictionary( entry.Value ).Value( "waitingtime_assignment_number" )
		            Dim waitDetetedFlag As String = Xojo.Core.Dictionary( entry.Value ).Value( "is_deleted" ) 
		            Dim waitUniqueID As String = Xojo.Core.Dictionary( entry.Value ).Value( "unique_record_identifier" )
		            'Dim waitUpdatable As Boolean = Get_xxx_Updatable( damUniqueID ) * Not needed for now
		            //Dim OwnerOrShared As Boolean = Ownership_And_Sharing( projNum ) ' Checks if the project sent is owned( created ) by the users company or is still shared by the the creator
		            ' Until the mobile App is modified
		            Dim OwnerOrShared As Boolean = True
		            
		            Dim assignStatusID As String = Get_Assignment_Status( assignNum )
		            If assignStatusID <> "4" And OwnerOrShared Then ' 4 = "Complété
		              
		              If activeAssignDic.HasKey( assignNum ) Then
		                Dim tempArr() As String = activeAssignDic.Value( assignNum )
		                tempArr.Append( waitUniqueID )
		                activeAssignDic.Value( assignNum ) = tempArr
		              Else
		                Dim inspArray() As String
		                inspArray.Append( waitUniqueID )
		                activeAssignDic.Value( assignNum ) = inspArray ' Key = assignNum and Value = Array( Inspection Unique record Identifier )
		              End If
		              
		              '//Effacement du WaitingTime
		              If  activeAssignDic.HasKey( assignNum ) And waitDetetedFlag = "1" Then  ' 0 = not deleted and 1 = deleted
		                message = Delete_Waitingtime( assignNum, waitUniqueID )
		                If message = App.kSuccess.ToText Then
		                  rUpdatedWaitingTime.Append( waitUniqueID )
		                Else
		                  '' Message à faire parvenir à l'administrateur
		                  SendMail( "Problem with deleting waitingtime" )
		                  jsonReturn.Value( "ReturnMessage" ) = message
		                  Request.Print( jsonReturn.ToString )
		                  disconnectDB
		                  Return True
		                End If
		              End If
		              
		              //Mise à jour du waitingtime
		              'If damUpdatable And activeAssignDic.HasKey( assignNum ) And damDetetedFlag = "0" Then ' 0 = not deleted and 1 = deleted
		              If activeAssignDic.HasKey( assignNum ) And waitDetetedFlag = "0" Then  ' 0 = not deleted and 1 = deleted
		                'Déterminer si on UPDATE ou INSERT
		                If WaitingtimeExists( waitUniqueID ) Then
		                  message = UpdateWaitingtime( Xojo.Core.Dictionary( entry.Value ) )
		                Else
		                  message = AddWaitingtime( Xojo.Core.Dictionary( entry.Value ) )
		                End If
		                
		                'Ajouter aux waitingtime modifiés
		                If message = App.kSuccess.ToText Then
		                  rUpdatedWaitingTime.Append( waitUniqueID )
		                Else
		                  // Message à faire parvenir à l'administrateur
		                  SendMail( "Problem With damages update" )
		                  jsonReturn.Value( "ReturnMessage" ) = message
		                  Request.Print( jsonReturn.ToString )
		                  disconnectDB
		                  Return True
		                End If
		              End If
		            End If
		          Next Entry
		          
		        End If
		        
		      Else
		        message = App.kSuccess
		      End If
		      
		      //Send back response with return data following this model
		      
		      ' {
		      ' "ReturnMessage": "Success",
		      ' "data": {
		      ' "repairs": ["a460693b-7f4d-4953-8fe3-d9cd06458e0a", "93b1ee01-b863-4145-a13e-8910034edcbe", "59562cc5-7369-476c-a5a1-a43e7cd50e9c"],
		      ' "repairsteps": ["59562cc5-7369-476c-a5a1-a43e7cd50e9c", "a460693b-7f4d-4953-8fe3-d9cd06458e0a", "93b1ee01-b863-4145-a13e-8910034edcbe"]
		      ' }
		      ' }
		      Dim returnDic As New Xojo.Core.Dictionary
		      returnDic.Value( "ReturnMessage" ) = Message
		      
		      'valeur de l'item data qui contient deux array soient celui des identifiants uniques des inspections mises à jour et celui des dentifiants uniques des dommages mis à jour
		      Dim rDataDic As New Xojo.Core.Dictionary
		      rDataDic.Value( "repairs" ) =  rUpdatedRepairs
		      rDataDic.Value( "repairsteps" ) =  rUpdatedSteps
		      rDataDic.Value( "waitingtimes" ) = rUpdatedWaitingTime
		      
		      returnDic.Value( "data" ) = Xojo.Data.GenerateJSON( rDataDic )
		      Request.Print( Xojo.Data.GenerateJSON( returnDic ) )
		      
		      disconnectDB
		      Return True
		    Case "getProjectAllData"
		      message = connectDB
		      If message <> App.kSuccess.ToText Then
		        // Message à faire parvenir à l'administrateur
		        //SendMail( message )
		        jsonReturn.Value( "ReturnMessage" ) = message
		        Request.Print( jsonReturn.ToString )
		        disconnectDB
		        Return True
		      End If
		      
		      //// Importer les infos de champs de base de données
		      //Dim jsonBd As Dictionary = Self.getAllTableFieldsInfo
		      //jsonReturn.Value( "AllTableFieldsInfo" ) = jsonBd
		      
		      //Identification de l'utilisateur
		      Dim inputDataJson As JsonItem
		      #If DebugBuild Then
		        inputDataJson = New JsonItem(  DefineEncoding( "{""user_company_code"":""MSI"",""user_id"":""41""}", Encodings.UTF8 ).ToText )
		      #Else
		        inputDataJson = New JsonItem(  DefineEncoding( Request.Entity, Encodings.UTF8 ).ToText )
		      #EndIf
		      
		      // Lecture des projets
		      Dim projectFieldDict As Dictionary =  App.readTableFields( "dbglobal","projects" )
		      Dim projectDataDict As Dictionary = ProjectsModule.getProjectData( inputDataJson, projectFieldDict )
		      message = projectDataDict.Value( "ReturnMessage" ).StringValue.ToText
		      If message <> App.kSuccess.ToText Then
		        // Message à faire parvenir à l'administrateur
		        SendMail( message )
		        jsonReturn.Value( "ReturnMessage" ) = message
		        Request.Print( jsonReturn.ToString )
		        disconnectDB
		        Return True
		      End If
		      jsonReturn.Value( "getProjectData" ) = projectDataDict.Lookup( "getProjectData","" )
		      
		      // Lecture des assignations
		      Dim assignmentFieldDict As Dictionary =  App.readTableFields( "dbglobal","assignments" )
		      Dim assignmentDataDict As Dictionary = AssignmentsModule.getAssignmentData( inputDataJson, assignmentFieldDict )
		      message = assignmentDataDict.Value( "ReturnMessage" ).StringValue.ToText
		      If message <> App.kSuccess.ToText Then
		        // Message à faire parvenir à l'administrateur
		        SendMail( message )
		        jsonReturn.Value( "ReturnMessage" ) = message
		        Request.Print( jsonReturn.ToString )
		        disconnectDB
		        Return True
		      End If
		      jsonReturn.Value( "getAssignmentData" ) = assignmentDataDict.Lookup( "getAssignmentData","" )
		      
		      // Lecture des paramètres
		      Dim jsonParametre As JSONItem
		      jsonParametre = ParametreModule.getParmsAll
		      message = jsonParametre.Value( "ReturnMessage" ).StringValue.ToText
		      //If message <> App.kSuccess.ToText Then
		      //// Message à faire parvenir à l'administrateur
		      //SendMail( message )
		      //jsonReturn.Value( "ReturnMessage" ) = message
		      //Request.Print( jsonReturn.ToString )
		      //disconnectDB
		      //Return True
		      //End If
		      jsonReturn.Value( "getParmsAll" ) = jsonParametre.Value( "getParmsAll" )
		      
		      // Lecture des sites
		      Dim jsonSites As JSONItem
		      jsonSites = OperatorsModule.getSitesAll
		      message = jsonSites.Value( "ReturnMessage" ).StringValue.ToText
		      //If message <> App.kSuccess.ToText Then
		      //// Message à faire parvenir à l'administrateur
		      //SendMail( message )
		      //jsonReturn.Value( "ReturnMessage" ) = message
		      //Request.Print( jsonReturn.ToString )
		      //disconnectDB
		      //Return True
		      //End If
		      jsonReturn.Value( "getSitesAll" ) = jsonSites.Value( "getSitesAll" )
		      
		      // Lecture des users
		      Dim jsonUsers As JSONItem
		      jsonUsers = UsersModule.getUsersAll( inputDataJson )
		      message = jsonUsers.Value( "ReturnMessage" ).StringValue.ToText
		      If message = App.kSuccess.ToText Then
		        //// Message à faire parvenir à l'administrateur
		        //SendMail( message )
		        //jsonReturn.Value( "ReturnMessage" ) = message
		        //Request.Print( jsonReturn.ToString )
		        //disconnectDB
		        //Return True
		        jsonReturn.Value( "getUsersAll" ) = jsonUsers.Value( "getUsersAll" )
		      Else
		        jsonReturn.Value( "getUsersAll" ) = ""
		      End If
		      
		      // Lecture des inspections
		      Dim jsonInspections As JSONItem
		      Dim dictGetAssignmentData As New Dictionary
		      If jsonReturn.Value( "getAssignmentData" ) <> Nil And jsonReturn.Value( "getAssignmentData" ) IsA Dictionary  Then
		        dictGetAssignmentData = jsonReturn.Value( "getAssignmentData" )
		      End If
		      jsonInspections = InspectionsModule.getInspectionsAll( dictGetAssignmentData )
		      message = jsonInspections.Value( "ReturnMessage" ).StringValue.ToText
		      //If message <> App.kSuccess.ToText Then
		      //// Message à faire parvenir à l'administrateur
		      //SendMail( message )
		      //Else
		      jsonReturn.Value( "getInspectionsAll" ) = jsonInspections.Value( "getInspectionsAll" )
		      //End If
		      
		      // Lecture des dommages des inspections
		      Dim jsonInspectionDamages As JSONItem
		      jsonInspectionDamages = InspectionsModule.getInspectionDamagesAll( dictGetAssignmentData )
		      message = jsonInspectionDamages.Value( "ReturnMessage" ).StringValue.ToText
		      //If message <> App.kSuccess.ToText Then
		      //// Message à faire parvenir à l'administrateur
		      //SendMail( message )
		      //Else
		      jsonReturn.Value( "getInspectionDamagesAll" ) = jsonInspectionDamages.Value( "getInspectionDamagesAll" )
		      //End If
		      
		      // Lecture des réparations
		      Dim jsonRepairs As JSONItem
		      If jsonReturn.Value( "getAssignmentData" ) <> Nil And jsonReturn.Value( "getAssignmentData" ) IsA Dictionary  Then
		        dictGetAssignmentData = jsonReturn.Value( "getAssignmentData" )
		      End If
		      jsonRepairs = RepairsModule.getRepairsAll( dictGetAssignmentData )
		      message = jsonRepairs.Value( "ReturnMessage" ).StringValue.ToText
		      //If message <> App.kSuccess.ToText Then
		      //// Message à faire parvenir à l'administrateur
		      //SendMail( message )
		      //Else
		      jsonReturn.Value( "getRepairsAll" ) = jsonRepairs.Value( "getRepairsAll" )
		      //End If
		      
		      // Lecture des étapes de réparation
		      Dim jsonRepairSteps As JSONItem
		      jsonRepairSteps = RepairsModule.getRepairStepsAll( dictGetAssignmentData )
		      message = jsonRepairSteps.Value( "ReturnMessage" ).StringValue.ToText
		      //If message <> App.kSuccess.ToText Then
		      // Message à faire parvenir à l'administrateur
		      //SendMail( message )
		      //Else
		      jsonReturn.Value( "getRepairStepsAll" ) = jsonRepairSteps.Value( "getRepairStepsAll" )
		      //End If
		      
		      // Lecture du temps d'attente
		      Dim waitingTimeFieldDict As Dictionary =  App.readTableFields( "dbglobal","waitingtime" )
		      Dim waitingTimeDataDict As Dictionary = WaitingTimeModule.GetWaitingTimeData( inputDataJson, waitingTimeFieldDict )
		      message = waitingTimeDataDict.Value( "ReturnMessage" ).StringValue.ToText
		      //If message <> App.kSuccess.ToText Then
		      //// Message à faire parvenir à l'administrateur
		      //SendMail( message )
		      //jsonReturn.Value( "ReturnMessage" ) = message
		      //Request.Print( jsonReturn.ToString )
		      //disconnectDB
		      //Return True
		      //End If
		      jsonReturn.Value( "getWaitingTimeAll" ) = waitingTimeDataDict.Lookup( "GetWaitingTimeData", "" )
		      
		      // Send back data
		      jsonReturn.Value( "ReturnMessage" ) = App.kSuccess
		      Request.Print( jsonReturn.ToString )
		      disconnectDB
		      Return True
		      
		    Case "windhubdata"
		      //message = transferPostToDB( Request )
		      If message = App.kSuccess.ToText Then
		        Request.Header( "Content-Type" ) = reponseContentTypeXML
		        Request.Print( ResponseSuccess )
		        // Lancement de la tâche WindhubProcess en arrière-Plan
		        message = soumettreTache.ToText
		        If message <> App.kSuccess.ToText Then
		          // Message à faire parvenir à l'administrateur
		          SendMail( message )
		        End If
		        Return True
		      Else
		        // Il y a eu un problème, transmettre le message à l'utilisateur et à l'administrateur du système
		        Request.Header( "Content-Type" ) = reponseContentTypeXML
		        Dim reponse As String = reponseProblemeDebut + message + reponseProblemeFin
		        Request.Print( reponse )
		        // Message à faire parvenir à l'administrateur
		        SendMail( message )
		        Return True
		      End
		      
		    Case Else
		      Request.Header( "Content-Type" ) = reponseContentTypeXML
		      Request.Print( reponseInvalidParam )
		      // Message à faire parvenir à l'administrateur
		      SendMail( "Erreur de parametrage" )
		      Return True
		    End Select
		    
		  End Select
		End Function
	#tag EndEvent


	#tag Method, Flags = &h0
		Function connectDB() As Text
		  Dim message As Text = App.kSuccess.ToText
		  
		  //Vérifie connexion à la BD
		  bdWindhub = New PostgreSQLDatabase
		  bdWindhub.Host = host
		  bdWindhub.UserName =USER
		  bdWindhub.Password = PASSWORD
		  bdWindhub.Port = port
		  bdWindhub.DatabaseName = centralDatabaseName
		  If Not (bdWindhub.Connect) Then
		    message = bdWindhub.ErrorMessage.ToText
		    Return message
		  Else
		    bdWindhub.SQLExecute("SET search_path TO form, operator, supplier, inspection, repair, dbglobal, portail, locking, audit, public;")
		    If  bdWindhub .Error Then
		      message = kERRORDB.ToText + bdWindhub.ErrorMessage.ToText
		      Return message
		    End If
		    
		  End If
		  
		  Return message
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function dBSafeSQL(TempString As String) As String
		  Dim goodString As String
		  
		  goodString = ReplaceAll(TempString, "'", "''")
		  
		  Return goodString
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DB_Connection()
		  'Initialize and verify if on Production or Test
		  //We'll use an INI settings file to read the environment value
		  
		  #if DebugBuild then
		    UserSettings = GetFolderItem( "").Parent.Child("ProdOuTest.ini")
		  #else
		    UserSettings = GetFolderItem("ProdOuTest.ini")
		  #Endif
		  
		  OpenINI(UserSettings)
		  
		  environnementProp = INI_File.Get("ProductionOuTest","Env","")
		  
		  host = HOSTPROD
		  centralDatabaseName = CENTRALDATABASENAMEPROD
		  port = 50000
		  If environnementProp = "NewRelease" Then  // Nous sommes en test
		    host = HOSTNEWRELEASE
		    centralDatabaseName = CENTRALDATABASENAMENEWRELEASE
		    port = 50000
		  Elseif environnementProp = "Dev" Then  // Nous sommes en Développement
		    host = HOSTDEV
		    centralDatabaseName = CENTRALDATABASENAMEDEV
		    port = 50000
		  ElseIf environnementProp = "Demo" Then  // Nous sommes en démo
		    host = HOSTDEMO
		    centralDatabaseName = CENTRALDATABASENAMEDEMO
		    port = 50000
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub disconnectDB()
		  bdWindhub.Close
		  bdWindhub = Nil
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getAllTableFieldsInfo() As Dictionary
		  Dim dictAllTableFieldsInfo As New Dictionary
		  
		  dictAllTableFieldsInfo.Value("projects") = App.readTableFields("projects","projects")
		  dictAllTableFieldsInfo.Value("parametre") = App.readTableFields("parametre","parametre")
		  
		  
		  Return dictAllTableFieldsInfo
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GMTextConversion(tempString As String) As String
		  Dim c as TextConverter
		  c=GetTextConverter(GetTextEncoding(&h500), GetTextEncoding(0))
		  return c.convert( tempString)
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub OpenINI(strFile as FolderItem)
		  INI_File=new INISettings(strFile)
		  INI_File.Load
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function readTableFields(schema As String, table_Name As String) As Dictionary
		  Dim tableRS As RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  Dim dictResults As New Dictionary
		  
		  //strSQL = "SELECT column_name, udt_name, character_maximum_length, numeric_precision, numeric_scale FROM information_schema.columns " + _
		  //"WHERE table_name = '" + table_Name + "' AND substring( column_name, 1, " + Str( Len( prefix ) ) + " ) = '" + prefix + "' Order By column_name" 
		  
		  strSQL = "SELECT attrelid::regclass As table_name" + _
		  ", attname AS column_name" + _
		  ", atttypid::regtype  AS datatype" + _
		  " FROM   pg_attribute" + _
		  " WHERE  attrelid = '" + schema + "." + table_Name + "'::regclass" + _
		  " AND    attnum > 0" + _
		  " AND    Not attisdropped" + _
		  " ORDER  BY attnum;"
		  
		  
		  tableRS =  App.BdWindHub.SQLSelect( strSQL )
		  compteur = tableRS.RecordCount
		  
		  //If tableRS.RecordCount = 0 Then 
		  //dictResults.Value( "ReturnMessage" ) = App.NOTABLEINFO
		  //Return dictResults
		  //End If
		  
		  Dim field As Dictionary
		  Dim table As New Dictionary
		  
		  While Not tableRS.EOF
		    field = New Dictionary
		    field.Value(  "column_name" ) = tableRS.Field(  "column_name" ).StringValue
		    field.Value(  "datatype" ) = tableRS.Field("datatype").StringValue
		    table.Value( tableRS.Field( "column_name" ).StringValue ) = field
		    tableRS.MoveNext
		  Wend
		  tableRS.Close
		  tableRS = Nil
		  
		  dictResults.Value( table_name ) = table
		  
		  
		  Return dictResults
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function resizePicture(inPicture as picture, newWidth as integer, newHeight as integer) As Picture
		  Dim r as new picture(newWidth, newHeight,32)
		  r.graphics.drawpicture inPicture, 0, 0, newWidth, newHeight, 0, 0, inPicture.width, inPicture.height
		  return r
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SendMail(messageAEnvoyer As String)
		  
		  SendMailSocket = New SMTP
		  SendMailSocket.Address = "mail.ml6.tech" // your SMTP email server
		  SendMailSocket.Port = 587 // Check your server for the property port # to use
		  SendMailSocket.ConnectionType = SendMailSocket.TLSv1
		  SendMailSocket.Username = "ml6.notification@ml6.tech"
		  SendMailSocket.Password = "@Ml612345@"
		  
		  // Create the actual email message
		  Dim mail As New EmailMessage
		  mail.FromAddress = "WindhubWebListener@ml6.tech"
		  mail.Subject = "Erreur générée par WindhubWebListener"
		  mail.BodyPlainText = DefineEncoding(GMTextConversion(messageAEnvoyer) , Encodings.UTF8)
		  mail.Headers.AppendHeader("X-Mailer", "Windhub Listener") // Sample header
		  mail.AddRecipient("ml6.notification@ml6.tech")
		  
		  // Add the message to the SMTPSocket and send it
		  SendMailSocket.Messages.Append(mail)
		  SendMailSocket.SendMail
		  //System.DebugLog "done"
		  
		  // Wait for the mail to finish sending before letting the Console app quit.
		  While Not SendMailSocket.Finished And Not SendMailSocket.Error
		    app.DoEvents
		  Wend
		  
		  SendMailSocket = Nil
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function soumettreTache() As String
		  Dim message As Text = App.kSuccess.ToText
		  Dim cmd As String = ""
		  Dim s As Shell
		  s = New Shell
		  
		  #If DebugBuild Then
		    cmd = "schtasks /Run  /TN WindhubProcess"
		    GoTo Execution
		  #Endif
		  
		  cmd = "schtasks /Run  /TN WindhubProcess"
		  If App.environnementProp = "Dev" Then  // Nous sommes en test
		    cmd = "schtasks /Run  /TN WindhubProcess"
		  Elseif App.environnementProp = "New Release" Then  // Nous sommes en test
		    cmd = "schtasks /Run  /TN WindhubProcess"
		  End If
		  
		  Execution:
		  s.Execute(cmd)
		  If s.ErrorCode <> 0 Then message = s.Result.ToText
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function utcTime() As date
		  dim dat as new date
		  dat.TotalSeconds = dat.TotalSeconds - (dat.GMTOffset * 3600)
		  Return dat
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		bdWindhub As PostgreSQLDatabase
	#tag EndProperty

	#tag Property, Flags = &h0
		centralDatabaseName As String
	#tag EndProperty

	#tag Property, Flags = &h0
		environnementProp As String
	#tag EndProperty

	#tag Property, Flags = &h0
		host As String
	#tag EndProperty

	#tag Property, Flags = &h0
		INI_File As INISettings
	#tag EndProperty

	#tag Property, Flags = &h21
		Private port As Integer = 50000
	#tag EndProperty

	#tag Property, Flags = &h0
		SendMailSocket As SMTP
	#tag EndProperty

	#tag Property, Flags = &h0
		UserSettings As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		xmlUpdateDoc As XmlDocument
	#tag EndProperty


	#tag Constant, Name = CENTRALDATABASENAMEDEMO, Type = String, Dynamic = True, Default = \"windhubdemo3_0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = CENTRALDATABASENAMEDEV, Type = String, Dynamic = True, Default = \"windhub_dev_3_0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = CENTRALDATABASENAMENEWRELEASE, Type = String, Dynamic = True, Default = \"windhub_nr_3_0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = CENTRALDATABASENAMEPROD, Type = String, Dynamic = True, Default = \"windhub1_1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = DATABASEOPEN, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Database open"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Base de donn\xC3\xA9es ouverture"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Database open"
	#tag EndConstant

	#tag Constant, Name = EmptyXML, Type = String, Dynamic = True, Default = \"<\?xml version\x3D\"1.0\" encoding\x3D\"UTF-8\"\?>\r<database>\r</database>", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTDEMO, Type = String, Dynamic = True, Default = \"localhost", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTDEV, Type = String, Dynamic = True, Default = \"69.28.83.192", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTNEWRELEASE, Type = String, Dynamic = True, Default = \"localhost", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTPROD, Type = String, Dynamic = True, Default = \"localhost", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kERRORDB, Type = String, Dynamic = False, Default = \"Database error :", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Database error :"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Database error :"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Erreur de base de donn\xC3\xA9es :"
		#Tag Instance, Platform = Any, Language = pt-BR, Definition  = \"Erro de base de dados : "
	#tag EndConstant

	#tag Constant, Name = kSuccess, Type = String, Dynamic = False, Default = \"Succes", Scope = Public
	#tag EndConstant

	#tag Constant, Name = NOTABLEINFO, Type = String, Dynamic = True, Default = \"Aucun projet dans la base de donn\xC3\xA9es", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No table info in the database"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Aucun info de table dans la base de donn\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No table info in the database"
	#tag EndConstant

	#tag Constant, Name = PASSWORD, Type = String, Dynamic = True, Default = \"ml6tech$&%", Scope = Public
	#tag EndConstant

	#tag Constant, Name = reponseContentTypeHTML, Type = String, Dynamic = True, Default = \"text/html; charset\x3Dutf-8", Scope = Public
	#tag EndConstant

	#tag Constant, Name = reponseContentTypeXML, Type = String, Dynamic = True, Default = \"application/xml; charset\x3D\"utf-8\"", Scope = Public
	#tag EndConstant

	#tag Constant, Name = reponseInvalidParam, Type = String, Dynamic = True, Default = \"<\?xml version\x3D\"1.0\" encoding\x3D\"UTF-8\" \?>\r<response>\r<success>true</success>\r<show_alert>true</show_alert>\r<title>Non trait\xC3\xA9 / Not processed </title>\r<message>Erreur de param\xC3\xA9trage!/parameter error!</message>\r<button_label>Ok</button_label>\r</response>", Scope = Public
	#tag EndConstant

	#tag Constant, Name = reponseProblemeDebut, Type = String, Dynamic = True, Default = \"<\?xml version\x3D\"1.0\" encoding\x3D\"UTF-8\" \?>\r<response>\r<success>false</success>\r<show_alert>true</show_alert>\r<title> Non trait\xC3\xA9 / Not processed </title>\r<message>", Scope = Public
	#tag EndConstant

	#tag Constant, Name = reponseProblemeFin, Type = String, Dynamic = True, Default = \"</message>\r<button_label>Ok</button_label>\r</response>", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ResponseSuccess, Type = String, Dynamic = True, Default = \"<\?xml version\x3D\"1.0\" encoding\x3D\"UTF-8\" \?>\r<response>\r<success>true</success>\r<show_alert>true</show_alert>\r<title>Envoy\xC3\xA9 / Sent </title>\r<message>Le formulaire a \xC3\xA9t\xC3\xA9 transmis avec succ\xC3\xA8s! / The form has been successfully posted!</message>\r<button_label>Ok</button_label>\r</response>", Scope = Public
	#tag EndConstant

	#tag Constant, Name = USER, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"ml6"
	#tag EndConstant

	#tag Constant, Name = VERSION, Type = String, Dynamic = True, Default = \"V03", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="centralDatabaseName"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="environnementProp"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="host"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
