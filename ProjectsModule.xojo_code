#tag Module
Protected Module ProjectsModule
	#tag Method, Flags = &h0
		Function getProjectData(inputDataJson_param As jSonItem, projectDataDict_param As Dictionary) As Dictionary
		  'Extract infos from inputJSON
		  Dim dictResults As New Dictionary
		  Dim user_company_code As String = inputDataJson_param.Lookup( "user_company_code","" )
		  mUserID = inputDataJson_param.Lookup( "user_id","" ).IntegerValue
		  
		  mProjects = Nil 
		  mProjects = New ProjectsModule.ProjectsClass( App.bdWindhub, user_company_code, mUserID )
		  Dim pRS As Recordset = mProjects.projectsRS
		  
		  Dim userRS As RecordSet
		  userRS = mProjects.loadDataByFieldIn( App.bdWindhub, "dbglobal.all_companies_view", "company_code", user_company_code, "company_name" )
		  
		  If userRS.RecordCount = 0 Then 
		    dictResults.Value( "ReturnMessage" ) = App.kSuccess
		    Return dictResults
		  End If
		  
		  mCompanyRole = userRS.Field( "company_role" ).StringValue
		  userRS.Close
		  userRS = Nil
		  
		  
		  //Dim pRS As RecordSet
		  //If userCompanyRole = "Operator" Then
		  //pRS = mProjects.loadDataByFieldIn( App.bdWindhub, "projects", "projects_operator_code", _
		  //user_company_code, "projects_number" )
		  //Else
		  //pRS = mProjects.loadDataByFieldIn( App.bdWindhub, "projects", "projects_supplier_code", _
		  //user_company_code, "projects_number" )
		  //End If
		  
		  
		  
		  If pRS.RecordCount = 0 Then 
		    dictResults.Value( "ReturnMessage" ) = App.kSuccess
		    Return dictResults
		  End If
		  
		  Dim dictProjects As New Dictionary
		  Dim projects As Dictionary
		  
		  While Not pRS.EOF
		    projects = New Dictionary
		    Dim dictLookup As Dictionary = projectDataDict_param.Lookup( "projects","" ) 
		    For i As Integer = 0 To dictLookup.Count - 1
		      
		      Dim fieldName As String = dictLookup.Key( i ).StringValue
		      
		      Dim propertyField As Dictionary = dictLookup.Lookup( fieldName,"" )
		      
		      Dim dataType As String = propertyField.Lookup( "datatype","" )
		      
		      Select Case dataType
		        
		      Case "boolean"
		        projects.Value( fieldName ) = Str( pRS.Field( fieldName ).BooleanValue )
		      Case "date"
		        If pRS.Field( fieldName ).DateValue <> Nil Then
		          projects.Value( fieldName ) = pRS.Field( fieldName ).StringValue
		        End If
		      Case "double"
		        projects.Value( fieldName ) = Str( pRS.Field( fieldName ).DoubleValue ) 
		      Case "integer"
		        projects.Value( fieldName ) = Str( pRS.Field( fieldName ).IntegerValue )
		      Case "smallint"
		        projects.Value( fieldName ) = Str( pRS.Field( fieldName ).IntegerValue )
		      Case "bigint"
		        projects.Value( fieldName ) = Str( pRS.Field( fieldName ).Int64Value )
		      Case "character varying"
		        projects.Value( fieldName ) = pRS.Field( fieldName ).StringValue
		      Case "text"
		        projects.Value( fieldName ) = pRS.Field( fieldName ).StringValue
		      Case "numeric"
		        projects.Value( fieldName ) = Str( pRS.Field( fieldName ).DoubleValue )
		      Case "uuid"
		        projects.Value( fieldName ) = pRS.Field( fieldName ).StringValue
		      Case "timestamp without time zone"
		        projects.Value( fieldName ) = pRS.Field( fieldName ).StringValue
		      Case "timestamp with time zone"
		        projects.Value( fieldName ) = pRS.Field( fieldName ).StringValue
		      Case "point"
		        projects.Value( fieldName ) = pRS.Field( fieldName ).StringValue
		      Case "double precision"
		        projects.Value( fieldName ) = Str( pRS.Field( fieldName ).DoubleValue ) 
		      End Select
		      
		      //Exceptions
		      projects.Value( "user_id" ) = Str( pRS.Field( "user_id" ).IntegerValue ) 
		      projects.Value( "projects_assignment_count" ) = CStr( mProjects.Count_Assignments_Per_Project( pRS.Field( "projects_number" ).StringValue,  mUserID ) )
		      
		    Next
		    
		    dictProjects.Value( str( pRS.Field( "projects_number" ).StringValue ) ) = projects
		    pRS.MoveNext
		  Wend
		  
		  pRS.Close
		  pRS = Nil
		  
		  dictResults.Value( "getProjectData" ) = dictProjects
		  dictResults.Value( "ReturnMessage" ) = App.kSuccess
		  
		  Return dictResults
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		mCompanyRole As String
	#tag EndProperty

	#tag Property, Flags = &h0
		mProjects As ProjectsModule.ProjectsClass
	#tag EndProperty

	#tag Property, Flags = &h0
		mUserID As Integer
	#tag EndProperty


	#tag Constant, Name = NOPROJECT, Type = String, Dynamic = True, Default = \"Aucun projet dans la base de donn\xC3\xA9es", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No project in the database"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Aucun projet dans la base de donn\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No project in the database"
	#tag EndConstant

	#tag Constant, Name = NOUSERSASSIGNEDTOPROJECTS, Type = String, Dynamic = True, Default = \"Aucun utilisateur asssign\xC3\xA9 dans la base de donn\xC3\xA9es", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No user assigned to projects in the database"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Aucun utilisateur asssign\xC3\xA9 dans la base de donn\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No user assigned to projects in the database"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mCompanyRole"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mUserID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
