#tag Class
Protected Class AssignmentsClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h0
		Sub Constructor(db As PostgresqlDatabase, company_code As String, userID As Integer)
		  Dim strSQL As String = "SELECT DISTINCT assign.assignments_created_by_user_id," + _
		  " assign.assignments_effective_end_date," + _
		  " assign.assignments_effective_start_date," + _
		  " assign.assignments_expected_end_date," + _
		  " assign.assignments_expected_start_date," + _
		  " assign.assignments_name," + _
		  " assign.assignments_number," + _
		  " assign.assignments_operator_code," + _
		  " assign.assignments_owner_code," + _
		  " assign.assignments_project_number," + _
		  " assign.assignments_shared," + _
		  " assign.assignments_site_id," + _
		  " assign.assignments_status_id," + _
		  " assign.assignments_supplier_code," + _
		  " assign.assignments_type_id," + _
		  " assign.assignments_year," + _
		  " assign.last_synced," + _
		  " assign.mobile_timestamp," + _
		  " assign.unique_record_identifier," + _
		  " waitingtime_activity_number," + _
		  " to_char(total_waitingtime, 'HH24:MI') AS total_waitingtime," + _
		  " assignments_users_user_id," + _
		  " ad.activity_number," + _
		  " ad.activity_uuid" + _
		  " FROM" + _
		  " dbglobal.all_projects_and_assignments_for_filtering_by_user_view As assign" + _
		  " INNER Join" + _
		  " (Select activity_number, activity_assignment_number,waitingtime_activity_number, total_waitingtime, total_waitingtime As assignments_total_waiting_duration, activity_uuid" + _
		  " FROM (((" + _
		  " (Select inspections_assignment_number As activity_assignment_number, inspections_inspection_num As activity_number, unique_record_identifier As activity_uuid FROM inspection.inspections" + _
		  " UNION" + _
		  " Select repairs.repairs_assignment_number As activity_assignment__number, repairs_repair_num As activity_number, unique_record_identifier As activity_uuid FROM repair.repairs) As ac" + _
		  " Left Join dbglobal.waitingtime ON (((ac.activity_assignment_number)::TEXT = (waitingtime_assignment_number)::TEXT) And (activity_number)::TEXT = waitingtime.waitingtime_activity_number )))" + _
		  " Left Join dbglobal.total_waitingtime_by_assignment_view ON (((waitingtime.waitingtime_assignment_number)::TEXT) = (total_waitingtime_by_assignment_view.waitingtime_assignment_number)))) As ad" + _
		  " ON assign.assignments_number = ad.activity_assignment_number" + _
		  " WHERE " + _
		  " ((assign.assignments_supplier_code = $1 AND ((assign.assignments_owner_code = $1) OR (assign.assignments_owner_code <> $1 AND assign.assignments_shared = TRUE)) " + _
		  " Or" + _
		  " ((assign.assignments_operator_code = $1) AND ((assign.assignments_owner_code = $1) OR (assign.assignments_owner_code <> $1 AND assign.assignments_shared = TRUE))))" + _
		  " And assign.assignments_users_user_id = $2)" + _
		  " ORDER BY assign.assignments_number;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare( strSQL )
		  pgps.Bind( 0, company_code )
		  pgps.Bind( 1, userID )
		  
		  If Not db.Error Then
		    assignmentsRS = pgps.SQLSelect
		  End If
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		assignmentsRS As RecordSet
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_assignment_number As String
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_created_by_user_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_effective_end_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_effective_start_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_expected_end_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_expected_start_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_name As String
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_number As String
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_operator_code As String
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_owner_code As String
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_shared As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_site_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_status_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_supplier_code As String
	#tag EndProperty

	#tag Property, Flags = &h0
		assignments_type_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		last_synced As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		mobile_timestamp As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		unique_record_identifier As String
	#tag EndProperty

	#tag Property, Flags = &h0
		user_id As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="assignments_assignment_number"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_created_by_user_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_effective_end_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_effective_start_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_expected_end_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_expected_start_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_name"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_number"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_operator_code"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_owner_code"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_shared"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_site_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_status_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_supplier_code"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="assignments_type_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="last_synced"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mobile_timestamp"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="unique_record_identifier"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="user_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
