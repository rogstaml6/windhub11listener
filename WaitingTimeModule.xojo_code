#tag Module
Protected Module WaitingTimeModule
	#tag Method, Flags = &h0
		Function AddWaitingtime(waitDic As Xojo.Core.Dictionary) As Text
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  Dim mobileUUID As String = waitDic.Lookup( "repairsteps_mobile_uuid","" )
		  
		  // Mise à jour des enregistrements
		  Dim strSQL As String = "INSERT INTO waitingtime" + _
		  " (  last_synced," + _
		  " mobile_timestamp," + _
		  " unique_record_identifier," + _
		  " waitingtime_activity_number," + _
		  " waitingtime_assignment_number," + _
		  " waitingtime_duration," + _ '6
		  " waitingtime_end_date_time," + _
		  " waitingtime_reason," + _
		  " waitingtime_remarks," + _
		  " waitingtime_start_date_time," + _
		  " waitingtime_team_on_site_date_time," + _
		  " waitingtime_waiting_number ) " + _
		  " VALUES(  $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12  ) ;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare( strSQL )
		  pgps.Bind( 0, CLong( waitDic.Lookup( "last_synced","0" ) ) )
		  pgps.Bind( 1, CLong( waitDic.Lookup( "mobile_timestamp","" ) ) )
		  pgps.Bind( 2, waitDic.Lookup( "unique_record_identifier","" ) )
		  pgps.Bind( 3, waitDic.Lookup( "waitingtime_activity_number","" ) )
		  pgps.Bind( 4, waitDic.Lookup( "waitingtime_assignment_number","" ) )
		  pgps.Bind( 5, waitDic.Lookup( "waitingtime_duration","00:00:00" ) )
		  pgps.Bind( 6, waitDic.Lookup( "waitingtime_end_date_time","" ) )
		  pgps.Bind( 7,  waitDic.Lookup( "waitingtime_reason","" ) )
		  pgps.Bind( 8,  waitDic.Lookup( "waitingtime_remarks","" ) )
		  pgps.Bind( 9,  waitDic.Lookup( "waitingtime_start_date_time","" ) )
		  pgps.Bind( 10,  waitDic.Lookup( "waitingtime_team_on_site_date_time","" ) )
		  pgps.Bind( 11,  Format( CDbl( CStr( waitDic.Lookup( "waitingtime_waiting_number","" ) ) ), "00" ) )
		  
		  dB.SQLExecute( "BEGIN TRANSACTION" )
		  pgps.SQLExecute
		  //Gestion Erreur
		  Dim message As Text
		  If dB.Error Then
		    dB.Rollback
		    message = dB.ErrorMessage.ToText
		  Else
		    dB.Commit
		    message = App.kSuccess.ToText
		  End If
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Delete_Waitingtime(assignNum As String, waitUniqueID As String) As Text
		  Dim dB As PostgreSQLDatabase = App.bdWindhub
		  Dim strSQL As String
		  
		  strSQL = "DELETE FROM dbglobal.waitingtime" + _
		  " WHERE unique_record_identifier = $1;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = dB.Prepare(strSQL)
		  pgps.Bind(0, waitUniqueID)
		  
		  dB.SQLExecute("BEGIN TRANSACTION")
		  pgps.SQLExecute
		  //Gestion Erreur
		  Dim message As Text
		  If dB.Error Then
		    message = dB.ErrorMessage.ToText
		    dB.Rollback
		  Else
		    dB.Commit
		    message = App.kSuccess.ToText
		  End If
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetWaitingTimeData(inputDataJson_param As jSonItem, waitingTimeDataDict_param As Dictionary) As Dictionary
		  
		  
		  Dim dictResults As New Dictionary
		  Dim user_company_code As String = inputDataJson_param.Lookup(  "user_company_code",""  )
		  //mUserID = inputDataJson_param.Lookup(  "user_id",""  ).IntegerValue
		  
		  mWaitingtime = Nil 
		  mWaitingtime = New WaitingtimeModule.WaitingtimeClass(  App.bdWindhub, user_company_code  )
		  Dim wRS As Recordset = mWaitingtime.waitingtimeRS
		  
		  Dim userRS As RecordSet
		  userRS = mWaitingtime.loadDataByFieldIn(  App.bdWindhub, "dbglobal.all_companies_view", "company_code", user_company_code, "company_name"  )
		  
		  If userRS.RecordCount = 0 Then 
		    dictResults.Value(  "ReturnMessage"  ) = App.kSuccess
		    Return dictResults
		  End If
		  
		  mCompanyRole = userRS.Field(  "company_role"  ).StringValue
		  userRS.Close
		  userRS = Nil
		  
		  
		  //Dim wRS As RecordSet
		  //If userCompanyRole = "Operator" Then
		  //wRS = mWaitingtime.loadDataByFieldIn(  App.bdWindhub, "waitingtime", "waitingtime_operator_code", _
		  //user_company_code, "waitingtime_number"  )
		  //Else
		  //wRS = mWaitingtime.loadDataByFieldIn(  App.bdWindhub, "waitingtime", "waitingtime_supplier_code", _
		  //user_company_code, "waitingtime_number"  )
		  //End If
		  
		  
		  
		  If wRS.RecordCount = 0 Then 
		    dictResults.Value(  "ReturnMessage"  ) = App.kSuccess
		    Return dictResults
		  End If
		  
		  Dim dictWaitingtime As New Dictionary
		  Dim waitingtime As Dictionary
		  
		  While Not wRS.EOF
		    waitingtime = New Dictionary
		    Dim dictLookup As Dictionary = waitingTimeDataDict_param.Lookup(  "waitingtime",""  ) 
		    For i As Integer = 0 To dictLookup.Count - 1
		      
		      Dim fieldName As String = dictLookup.Key( i ).StringValue
		      
		      Dim propertyField As Dictionary = dictLookup.Lookup( fieldName,"" )
		      
		      Dim dataType As String = propertyField.Lookup( "datatype","" )
		      
		      Select Case dataType
		        
		      Case "boolean"
		        waitingtime.Value( fieldName ) = Str( wRS.Field( fieldName ).BooleanValue )
		      Case "date"
		        If wRS.Field( fieldName ).DateValue <> Nil Then
		          waitingtime.Value( fieldName ) = wRS.Field( fieldName ).DateValue.SQLDateTime
		        End If
		      Case "double"
		        waitingtime.Value( fieldName ) = Str( wRS.Field( fieldName ).DoubleValue ) 
		      Case "integer"
		        waitingtime.Value( fieldName ) = Str( wRS.Field( fieldName ).IntegerValue )
		      Case "smallint"
		        waitingtime.Value( fieldName ) = Str( wRS.Field( fieldName ).IntegerValue )
		      Case "bigint"
		        waitingtime.Value( fieldName ) = Str( wRS.Field( fieldName ).Int64Value )
		      Case "character varying"
		        waitingtime.Value( fieldName ) = wRS.Field( fieldName ).StringValue
		      Case "text"
		        waitingtime.Value( fieldName ) = wRS.Field( fieldName ).StringValue
		      Case "numeric"
		        waitingtime.Value( fieldName ) = Str( wRS.Field( fieldName ).DoubleValue )
		      Case "uuid"
		        waitingtime.Value( fieldName ) = wRS.Field( fieldName ).StringValue
		      Case "timestamp without time zone"
		        waitingtime.Value( fieldName ) = wRS.Field( fieldName ).StringValue
		      Case "timestamp with time zone"
		        waitingtime.Value( fieldName ) = wRS.Field( fieldName ).StringValue
		      Case "point"
		        waitingtime.Value( fieldName ) = wRS.Field( fieldName ).StringValue
		      Case "double precision"
		        waitingtime.Value( fieldName ) = Str( wRS.Field( fieldName ).DoubleValue ) 
		      End Select
		      
		    Next
		    
		    Dim strWTK As String =  wRS.Field( "waitingtime_assignment_number" ).StringValue + " " +  Str( wRS.Field( "waitingtime_activity_number" ).IntegerValue, "000" ) + " " +Str( wRS.Field( "waitingtime_waiting_number").IntegerValue, "00" )
		    
		    dictWaitingtime.Value( strWTK ) = waitingtime
		    
		    wRS.MoveNext
		  Wend
		  
		  wRS.Close
		  wRS = Nil
		  
		  dictResults.Value(  "getWaitingtimeData"  ) = dictWaitingtime
		  dictResults.Value(  "ReturnMessage"  ) = App.kSuccess
		  
		  Return dictResults
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UpdateWaitingtime(waitingtimeDic As Xojo.Core.Dictionary) As Text
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  Dim waitUniqueID As String = waitingtimeDic.Lookup( "unique_record_identifier","" )
		  
		  Dim strSQL As String = ""
		  
		  // Modification de l'enregistrements correspondants au projet et réparation
		  strSQL = "UPDATE waitingtime SET" + _
		  " last_synced = $2," + _
		  " mobile_timestamp = $3," + _
		  " waitingtime_activity_number = $4," + _
		  " waitingtime_assignment_number = $5," + _
		  " waitingtime_duration = $6," + _
		  " waitingtime_end_date_time = $7," + _
		  " waitingtime_reason = $8," + _
		  " waitingtime_remarks = $9," + _
		  " waitingtime_start_date_time = $10," + _
		  " waitingtime_team_on_site_date_time = $11," + _
		  " waitingtime_waiting_number = $12" + _
		  " WHERE" + _
		  " unique_record_identifier = $1;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = dB.Prepare( strSQL )
		  pgps.Bind( 0, waitUniqueID )
		  pgps.Bind( 1, CLong( waitingtimeDic.Lookup( "last_synced","0" ) ) )
		  pgps.Bind( 2, CLong( waitingtimeDic.Lookup( "mobile_timestamp","" ) ) )
		  pgps.Bind( 3, waitingtimeDic.Lookup( "waitingtime_activity_number","" ) )
		  pgps.Bind( 4, waitingtimeDic.Lookup( "waitingtime_assignment_number","" ) )
		  pgps.Bind( 5, waitingtimeDic.Lookup( "waitingtime_duration","00:00:00" ) )
		  pgps.Bind( 6, waitingtimeDic.Lookup( "waitingtime_end_date_time","" ) )
		  pgps.Bind( 7, waitingtimeDic.Lookup( "waitingtime_reason","" ) )
		  pgps.Bind( 8, waitingtimeDic.Lookup( "waitingtime_remarks","" ) )
		  pgps.Bind( 9, waitingtimeDic.Lookup( "waitingtime_start_date_time","" ) )
		  pgps.Bind( 10, waitingtimeDic.Lookup( "waitingtime_team_on_site_date_time","" ) )
		  pgps.Bind( 11,  Format( CDbl( CStr( waitingtimeDic.Lookup( "waitingtime_waiting_number","" ) ) ), "00" ) )
		  
		  dB.SQLExecute( "BEGIN TRANSACTION" )
		  pgps.SQLExecute
		  //Gestion Erreur
		  Dim message As Text
		  If dB.Error Then
		    db.Rollback
		    message = db.ErrorMessage.ToText
		  Else
		    dB.Commit
		    message = App.kSuccess.ToText
		  End If
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function WaitingtimeExists(waitUniqueID As String) As Boolean
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  
		  Dim strSQL As String = "SELECT count(unique_record_identifier)" + _
		  " FROM dbglobal.waitingtime" + _
		  " WHERE unique_record_identifier = $1;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare(strSQL)
		  pgps.Bind(0, waitUniqueID)
		  
		  Dim RS As RecordSet = pgps.SQLSelect
		  
		  If Not (RS.BOF And RS.EOF) And RS.RecordCount > 0 Then
		    
		    Select Case RS.Field("count").IntegerValue
		    Case Is = 0
		      Return False
		    Case Is > 0
		      Return True
		    End Select
		    
		  End If
		  
		  Return False
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		mWaitingTime As WaitingTimeModule.WaitingTimeClass
	#tag EndProperty


	#tag Constant, Name = NOWAITINGTIME, Type = String, Dynamic = True, Default = \"Aucun remps d\'attente dans la base de donn\xC3\xA9es", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No waiting time in the database"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Aucun temps d\'attente dans la base de donn\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No waiting time in the database"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
