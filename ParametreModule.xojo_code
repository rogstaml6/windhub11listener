#tag Module
Protected Module ParametreModule
	#tag Method, Flags = &h0
		Function getParmsAll() As JSONItem
		  
		  mParametres = Nil 
		  mParametres = new ParametreClass()
		  
		  Dim parametreRS As RecordSet
		  parametreRS = mParametres.loadDataByFieldCond( App.bdWindhub, "parametre", "parametre_statut", "=", "A", "", "", "", "", "parametre_nom", "parametre_tri")
		  
		  If parametreRS.RecordCount = 0 Then 
		    Dim jsonError As New JSONItem
		    jsonError.Value("ReturnMessage") = ParametreModule.NOPAREMETREDB
		    Return jsonError
		  End If
		  
		  Dim jsonParametres As New JSONItem
		  Dim parametre As Dictionary
		  
		  While Not parametreRS.EOF
		    parametre = New Dictionary
		    parametre.Value("parametre_nom") = parametreRS.Field("parametre_nom").StringValue
		    parametre.Value("parametre_tri") = parametreRS.Field("parametre_tri").IntegerValue
		    parametre.Value("parametre_cle") = parametreRS.Field("parametre_cle").StringValue
		    parametre.Value("parametre_desc_fr") = parametreRS.Field("parametre_desc_fr").StringValue
		    parametre.Value("parametre_desc_en") = parametreRS.Field("parametre_desc_en").StringValue
		    parametre.Value("parametre_desc_pt") = parametreRS.Field("parametre_desc_pt").StringValue
		    //parametre.Value("parametre_desc_es") = parametreRS.Field("parametre_desc_es").StringValue
		    parametre.Value("parametre_cleetr_nom") = parametreRS.Field("parametre_cleetr_nom").StringValue
		    parametre.Value("parametre_cleetr_cle") = parametreRS.Field("parametre_cleetr_cle").StringValue
		    parametre.Value("parametre_statut") = parametreRS.Field("parametre_statut").StringValue
		    parametre.Value("parametre_field_name") = parametreRS.Field("parametre_field_name").StringValue
		    parametre.Value("parametre_unit_metric") = parametreRS.Field("parametre_unit_metric").StringValue
		    jsonParametres.Value(str(parametreRS.Field("parametre_id").IntegerValue)) = parametre
		    parametreRS.MoveNext
		  Wend
		  
		  parametreRS.Close
		  
		  // Blade manufacturers est chargé comme faisant partie de la table paramètre
		  parametreRS = mParametres.loadData(App.bdWindhub, "blademanufacturers", "blademanufacturers_id")
		  
		  //Ajout de l'entrée "TITLE"
		  If parametreRS <> Nil And parametreRS.RecordCount > 0 Then
		    parametre = New Dictionary
		    parametre.Value("parametre_nom") = "blademanufacturers"
		    parametre.Value("parametre_tri") = "0"
		    parametre.Value("parametre_cle") = "Title"
		    parametre.Value("parametre_desc_fr") = "Blade manufacturer"
		    parametre.Value("parametre_desc_en") = "Fabricant de pales"
		    parametre.Value("parametre_desc_pt") = "Fabricante da Pá"
		    //parametre.Value("parametre_desc_es") = "Fabricante de Palas"
		    parametre.Value("parametre_cleetr_nom") = "noselection"
		    parametre.Value("parametre_cleetr_cle") = "1"
		    parametre.Value("parametre_statut") = "A"
		    parametre.Value("parametre_field_name") = "blade_manufacturer"
		    jsonParametres.Value("bm" + "0") = parametre
		  End If
		  
		  While Not parametreRS.EOF
		    parametre = New Dictionary
		    parametre.Value("parametre_nom") = "blademanufacturers"
		    parametre.Value("parametre_tri") = parametreRS.Field("blademanufacturers_id").IntegerValue
		    parametre.Value("parametre_cle") = parametreRS.Field("blademanufacturers_code").StringValue
		    parametre.Value("parametre_desc_fr") = parametreRS.Field("blademanufacturers_name").StringValue
		    parametre.Value("parametre_desc_en") = parametreRS.Field("blademanufacturers_name").StringValue
		    parametre.Value("parametre_desc_pt") = parametreRS.Field("blademanufacturers_name").StringValue
		    //parametre.Value("parametre_desc_es") = parametreRS.Field("blademanufacturers_name").StringValue
		    parametre.Value("parametre_cleetr_nom") = "noselection"
		    parametre.Value("parametre_cleetr_cle") = "1"
		    parametre.Value("parametre_statut") = "A"
		    parametre.Value("parametre_field_name") = "blade_manufacturer"
		    jsonParametres.Value("bm" + Str(parametreRS.Field("blademanufacturers_id").IntegerValue)) = parametre
		    parametreRS.MoveNext
		  Wend
		  
		  parametreRS.Close
		  parametreRS = Nil
		  
		  Dim jsonResults As New JSONItem
		  jsonResults.Value("getParmsAll") = jsonParametres
		  jsonResults.Value("ReturnMessage") = App.kSuccess
		  
		  Return jsonResults
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		mParametres As ParametreModule.ParametreClass
	#tag EndProperty


	#tag Constant, Name = NOPAREMETREDB, Type = String, Dynamic = True, Default = \"No corresponding parameter in the database", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
