#tag Module
Protected Module InspectionsModule
	#tag Method, Flags = &h0
		Function AddInspectionDamage(damageDic As Xojo.Core.Dictionary) As Text
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  Dim mobileUUID As String = damageDic.Lookup( "inspectiondamages_mobile_uuid","" )
		  
		  // Mise à jour des enregistrements
		  Dim strSQL As String = "INSERT INTO inspection.inspectiondamages" + _
		  " (  inspectiondamages_area," + _ '$1
		  " inspectiondamages_assignment_number," + _
		  " inspectiondamages_damage_desc," + _
		  " inspectiondamages_damage_num," + _
		  " inspectiondamages_damage_radius," + _
		  " inspectiondamages_damage_severity," + _
		  " inspectiondamages_damage_type," + _
		  " inspectiondamages_date," + _
		  " inspectiondamages_inspection_num," + _
		  " inspectiondamages_length," + _
		  " inspectiondamages_mobile_uuid," + _
		  " inspectiondamages_photo_desc01," + _ '$11
		  " inspectiondamages_photo_desc02," + _
		  " inspectiondamages_position_edge," + _
		  " inspectiondamages_position_side," + _
		  " inspectiondamages_remark," + _
		  " inspectiondamages_section," + _
		  " inspectiondamages_updatable," + _ '$18
		  " inspectiondamages_web," + _
		  " last_synced," + _
		  " mobile_timestamp," + _
		  " unique_record_identifier )" + _
		  " VALUES (  $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22  );"
		  
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare( strSQL )
		  pgps.Bind( 0, damageDic.Lookup( "inspectiondamages_area","" ) )
		  pgps.Bind( 1, damageDic.Lookup( "inspectiondamages_assignment_number","" ) )
		  pgps.Bind( 2, damageDic.Lookup( "inspectiondamages_damage_desc","" ) )
		  pgps.Bind( 3,  Format( CDbl( damageDic.Lookup( "inspectiondamages_damage_num","" ) ), "00" ) )
		  pgps.Bind( 4, damageDic.Lookup( "inspectiondamages_damage_radius","" ) )
		  pgps.Bind( 5, damageDic.Lookup( "inspectiondamages_damage_severity","" ) )
		  pgps.Bind( 6, damageDic.Lookup( "inspectiondamages_damage_type","" ) )
		  pgps.Bind( 7, damageDic.Lookup( "inspectiondamages_date","" ) )
		  pgps.Bind( 8, Format( CDbl( damageDic.Lookup( "inspectiondamages_inspection_num","" ) ),  "000" ) )
		  pgps.Bind( 9, damageDic.Lookup( "inspectiondamages_length","" ) )
		  pgps.Bind( 10, Trim(  damageDic.Lookup( "inspectiondamages_mobile_uuid","" ) ) )
		  pgps.Bind( 11, damageDic.Lookup( "inspectiondamages_photo_desc01","" ) )
		  pgps.Bind( 12, damageDic.Lookup( "inspectiondamages_photo_desc02","" ) )
		  pgps.Bind( 13, damageDic.Lookup( "inspectiondamages_position_edge","" ) )
		  pgps.Bind( 14, damageDic.Lookup( "inspectiondamages_position_side","" ) )
		  pgps.Bind( 15, damageDic.Lookup( "inspectiondamages_remark","" ) )
		  pgps.Bind( 16, damageDic.Lookup( "inspectiondamages_section","" ) )
		  pgps.Bind( 17, damageDic.Lookup( "inspectiondamages_updatable","True" ) )
		  pgps.Bind( 18, damageDic.Lookup( "inspectiondamages_web","" ) )
		  pgps.Bind( 19, CLong( damageDic.Lookup( "last_synced","" ) ) )
		  pgps.Bind( 20, CLong( damageDic.Lookup( "mobile_timestamp","" ) ) )
		  pgps.Bind( 21, damageDic.Lookup( "unique_record_identifier","" ) )
		  
		  dB.SQLExecute( "BEGIN TRANSACTION" )
		  pgps.SQLExecute
		  //Gestion Erreur
		  Dim message As Text
		  If dB.Error Then
		    dB.Rollback
		    message = dB.ErrorMessage.ToText
		  Else
		    dB.Commit
		    message = App.kSuccess.ToText
		  End If
		  
		  Dim assignNum As String = damageDic.Lookup( "inspectiondamages_assignment_number","" )
		  Dim damUniqueID As String = damageDic.Lookup( "unique_record_identifier","" )
		  
		  //Mettre à jour les photos
		  If message = App.kSuccess.ToText Then
		    Update_Damage_Photos( damageDic, assignNum, damUniqueID )
		  End If
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function damageExists(damageUniqueID As String) As Boolean
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  
		  Dim strSQL As String = "SELECT count(unique_record_identifier)" + _
		  " FROM inspection.inspectiondamages" + _
		  " WHERE unique_record_identifier = $1;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare(strSQL)
		  pgps.Bind(0, damageUniqueID)
		  
		  Dim RS As RecordSet = pgps.SQLSelect
		  
		  If Not (RS.BOF And RS.EOF) And RS.RecordCount > 0 Then
		    
		    Select Case RS.Field("count").IntegerValue
		    Case Is = 0
		      Return False
		    Case Is > 0
		      Return True
		    End Select
		    
		  End If
		  
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Delete_Damages(assignNum As String, damUniqueID As String) As Text
		  Dim dB As PostgreSQLDatabase = App.bdWindhub
		  Dim strSQL As String
		  
		  // Suppression de l'enregistrement correspondant
		  strSQL = "DELETE FROM inspection.inspectiondamages" + _
		  " WHERE unique_record_identifier = $1" + _
		  "AND inspectiondamages_updatable = TRUE"
		  
		  Dim pgps As PostgreSQLPreparedStatement = dB.Prepare(strSQL)
		  pgps.Bind(0, damUniqueID)
		  
		  dB.SQLExecute("BEGIN TRANSACTION")
		  pgps.SQLExecute
		  //Gestion Erreur
		  Dim message As Text
		  If dB.Error Then
		    message = dB.ErrorMessage.ToText
		    dB.Rollback
		  Else
		    dB.Commit
		    message = App.kSuccess.ToText
		  End If
		  
		  Return message
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function FindPhotoExtention(fileNameToCheck As String) As String
		  //On ne teste pas l'ouverture qui serait plus fiable car ça ralentirait trop le processus
		  
		  Dim extArray() As String
		  extArray = Split(kPhotoExtentionsList, ",")
		  
		  For i As Integer = 0 To extArray.Ubound
		    Dim photoFile As FolderItem
		    //dim x As String = damPhotoFolder.NativePath + year + "\" + fileNameToCheck + "." + extArray(i)
		    //photoFile = GetFolderItem(damPhotoFolder.NativePath + App.mProjInfo.year + "\" + fileNameToCheck + "." + extArray(i))
		    
		    If photoFile.Exists Then
		      Return photoFile.Name
		    End If
		    
		  Next i
		  
		  Return ""
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getInspectionDamagesAll(inputDataDict_param As Dictionary) As JSONItem
		  
		  mInspectionDamages = Nil 
		  mInspectionDamages = New InspectionDamagesClass
		  
		  Dim inspectionDamagesRS As RecordSet
		  inspectionDamagesRS = mInspectionDamages.loadData(App.bdWindhub, "inspectiondamages", "inspectiondamages_assignment_number", "inspectiondamages_inspection_num", "inspectiondamages_damage_num")
		  Dim jsonInspectionDamages As New JSONItem
		  Dim inspectionDamages As Dictionary
		  
		  If inspectionDamagesRS.RecordCount = 0 Or inputDataDict_param.Count  = 0 Then 
		    Dim jsonError As New JSONItem
		    jsonError.Value( "ReturnMessage" ) = InspectionsModule.AUCUNEINSPECTION
		    jsonError.Value( "getInspectionDamagesAll" ) = ""
		    Return jsonError
		  End If
		  
		  While Not inspectionDamagesRS.EOF
		    
		    Dim assignNum As String = inspectionDamagesRS.Field( "inspectiondamages_assignment_number" ).StringValue
		    Dim inspNum As String = inspectionDamagesRS.Field( "inspectiondamages_inspection_num" ).StringValue
		    
		    If Not inputDataDict_param.HasKey( assignNum + " " + inspNum ) Then GoTo Suivant
		    
		    inspectionDamages = New Dictionary
		    inspectionDamages.Value("inspectiondamages_assignment_number") =inspectionDamagesRS.Field("inspectiondamages_assignment_number").StringValue
		    inspectionDamages.Value("inspectiondamages_inspection_num") =inspectionDamagesRS.Field("inspectiondamages_inspection_num").StringValue
		    inspectionDamages.Value("inspectiondamages_damage_num") =inspectionDamagesRS.Field("inspectiondamages_damage_num").StringValue
		    inspectionDamages.Value("inspectiondamages_date") =inspectionDamagesRS.Field("inspectiondamages_date").StringValue
		    inspectionDamages.Value("inspectiondamages_damage_type") =inspectionDamagesRS.Field("inspectiondamages_damage_type").StringValue
		    inspectionDamages.Value("inspectiondamages_damage_desc") =inspectionDamagesRS.Field("inspectiondamages_damage_desc").StringValue
		    inspectionDamages.Value("inspectiondamages_damage_severity") =inspectionDamagesRS.Field("inspectiondamages_damage_severity").StringValue
		    inspectionDamages.Value("inspectiondamages_damage_radius") =  ReplaceAll(str(Format(inspectionDamagesRS.Field("inspectiondamages_damage_radius").DoubleValue, "#.00")),",",".")
		    If inspectionDamagesRS.Field("inspectiondamages_damage_radius").DoubleValue = 0 Then inspectionDamages.Value("inspectiondamages_damage_radius") = ""
		    inspectionDamages.Value("inspectiondamages_position_side") =inspectionDamagesRS.Field("inspectiondamages_position_side").StringValue
		    inspectionDamages.Value("inspectiondamages_position_edge") =inspectionDamagesRS.Field("inspectiondamages_position_edge").StringValue
		    inspectionDamages.Value("inspectiondamages_area") =inspectionDamagesRS.Field("inspectiondamages_area").StringValue
		    inspectionDamages.Value("inspectiondamages_area") =  ReplaceAll(str(Format(inspectionDamagesRS.Field("inspectiondamages_area").DoubleValue, "#.00")),",",".")
		    If inspectionDamagesRS.Field("inspectiondamages_area").DoubleValue = 0 Then inspectionDamages.Value("inspectiondamages_area") = ""
		    inspectionDamages.Value("inspectiondamages_web") =inspectionDamagesRS.Field("inspectiondamages_web").StringValue
		    inspectionDamages.Value("inspectiondamages_section") =inspectionDamagesRS.Field("inspectiondamages_section").StringValue
		    inspectionDamages.Value("inspectiondamages_length") =  ReplaceAll(str(Format(inspectionDamagesRS.Field("inspectiondamages_length").DoubleValue, "#.00")),",",".")
		    If inspectionDamagesRS.Field("inspectiondamages_length").DoubleValue = 0 Then inspectionDamages.Value("inspectiondamages_length") = ""
		    inspectionDamages.Value("inspectiondamages_remark") =inspectionDamagesRS.Field("inspectiondamages_remark").StringValue
		    inspectionDamages.Value("inspectiondamages_photo_desc01") =inspectionDamagesRS.Field("inspectiondamages_photo_desc01").StringValue
		    inspectionDamages.Value("inspectiondamages_photo_desc02") =inspectionDamagesRS.Field("inspectiondamages_photo_desc02").StringValue
		    inspectionDamages.Value("inspectiondamages_updatable") = inspectionDamagesRS.Field("inspectiondamages_updatable").StringValue
		    inspectionDamages.Value("mobile_timestamp") = inspectionDamagesRS.Field("mobile_timestamp").IntegerValue
		    inspectionDamages.Value("unique_record_identifier") = inspectionDamagesRS.Field("unique_record_identifier").StringValue
		    inspectionDamages.Value("last_synced") = inspectionDamagesRS.Field("last_synced").IntegerValue
		    jsonInspectionDamages.Value(inspectionDamagesRS.Field("inspectiondamages_assignment_number").StringValue + " " + inspectionDamagesRS.Field("inspectiondamages_inspection_num").StringValue + _
		    " " + inspectionDamagesRS.Field("inspectiondamages_damage_num").StringValue) = inspectionDamages
		    
		    'Set inspection user_id based on the  fact that the inspection is linked to an assignment performed by the user
		    inspectionDamages.Value( "user_id" ) = CStr( mUserID )
		    
		    Suivant:
		    inspectionDamagesRS.MoveNext
		  Wend
		  
		  
		  inspectionDamagesRS.Close
		  inspectionDamagesRS = Nil
		  
		  Dim jsonResults As New JSONItem
		  jsonResults.Value("getInspectionDamagesAll") = jsonInspectionDamages
		  jsonResults.Value("ReturnMessage") = App.kSuccess
		  
		  Return jsonResults
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getInspectionsAll(inputDataDict_param As Dictionary) As JSONItem
		  Dim S As Session = Session
		  
		  mInspection = Nil 
		  mInspection = New InspectionsClass
		  
		  Dim inspectionRS As RecordSet
		  inspectionRS = mInspection.loadData( App.bdWindhub, "inspections", "inspections_assignment_number", "inspections_inspection_num" )
		  
		  If inspectionRS.RecordCount = 0  Or inputDataDict_param.Count = 0 Then 
		    Dim jsonError As New JSONItem
		    jsonError.Value( "ReturnMessage" ) = InspectionsModule.AUCUNEINSPECTION
		    jsonError.Value( "getInspectionsAll" ) = ""
		    Return jsonError
		  End If
		  
		  
		  //Dim projectDict As Dictionary = inputDataJson_param.Value( "getProjectData" )
		  Dim jsonInspections As New JSONItem
		  Dim inspection As Dictionary
		  
		  While Not inspectionRS.EOF
		    Dim assignNum As String = inspectionRS.Field( "inspections_assignment_number" ).StringValue
		    Dim inspNum As String = inspectionRS.Field( "inspections_inspection_num" ).StringValue
		    
		    If Not inputDataDict_param.HasKey( assignNum + " " + inspNum ) Then GoTo Suivant
		    
		    inspection = New Dictionary
		    inspection.Value( "inspections_assignment_number" ) = inspectionRS.Field( "inspections_assignment_number" ).StringValue
		    inspection.Value( "inspections_inspection_num" ) = inspectionRS.Field( "inspections_inspection_num" ).StringValue
		    inspection.Value( "inspections_wtg_num" ) = inspectionRS.Field( "inspections_wtg_num" ).StringValue
		    inspection.Value( "inspections_wtg_id" ) = Str( inspectionRS.Field( "inspections_wtg_id" ).IntegerValue )
		    inspection.Value( "inspections_blade_num" ) = inspectionRS.Field( "inspections_blade_num" ).StringValue
		    inspection.Value( "inspections_blade_serial" ) = inspectionRS.Field( "inspections_blade_serial" ).StringValue
		    inspection.Value( "inspections_int_ext" ) = inspectionRS.Field( "inspections_int_ext" ).StringValue
		    inspection.Value( "inspections_access_method" ) = inspectionRS.Field( "inspections_access_method" ).StringValue
		    inspection.Value( "inspections_priority" ) = inspectionRS.Field( "inspections_priority" ).StringValue
		    inspection.Value( "inspections_progress_status" ) = inspectionRS.Field( "inspections_progress_status" ).StringValue
		    inspection.Value( "inspections_start_date" ) = inspectionRS.Field( "inspections_start_date" ).StringValue
		    inspection.Value( "inspections_end_date" ) = inspectionRS.Field( "inspections_end_date" ).StringValue
		    inspection.Value( "inspections_time_estimated" ) = ReplaceAll( str( Format( inspectionRS.Field( "inspections_time_estimated" ).DoubleValue, "#.00" ) ),",","." )
		    If inspectionRS.Field( "inspections_time_estimated" ).DoubleValue = 0 Then inspection.Value( "inspections_time_estimated" ) = ""
		    inspection.Value( "inspections_time_spent" ) = ReplaceAll( str( Format( inspectionRS.Field( "inspections_time_spent" ).DoubleValue, "#.00" ) ),",","." )
		    If inspectionRS.Field( "inspections_time_spent" ).DoubleValue = 0 Then inspection.Value( "inspections_time_spent" ) = ""
		    inspection.Value( "inspections_remark" ) = inspectionRS.Field( "inspections_remark" ).StringValue
		    inspection.Value( "inspections_blade_manufacturer" ) = inspectionRS.Field( "inspections_blade_manufacturer" ).StringValue
		    inspection.Value( "inspections_blade_length" ) = inspectionRS.Field( "inspections_blade_length" ).StringValue
		    inspection.Value( "inspections_radius1" ) =  ReplaceAll( Str( Format( inspectionRS.Field( "inspections_radius1" ).DoubleValue, "#.00" ) ),",","." )
		    If inspectionRS.Field( "inspections_radius1" ).DoubleValue = 0 Then inspection.Value( "inspections_radius1" ) = ""
		    inspection.Value( "inspections_radius1_ps" ) =  ReplaceAll( Str( Format( inspectionRS.Field( "inspections_radius1_ps" ).DoubleValue, "#.00" ) ),",","." )
		    If inspectionRS.Field( "inspections_radius1_ps" ).DoubleValue = 0 Then inspection.Value( "inspections_radius1_ps" ) = ""
		    inspection.Value( "inspections_radius1_ss" ) =  ReplaceAll( str( Format( inspectionRS.Field( "inspections_radius1_ss" ).DoubleValue, "#.00" ) ),",","." )
		    If inspectionRS.Field( "inspections_radius1_ss" ).DoubleValue = 0 Then inspection.Value( "inspections_radius1_ss" ) = ""
		    inspection.Value( "inspections_radius2" ) = ReplaceAll( str( Format( inspectionRS.Field( "inspections_radius2" ).DoubleValue, "#.00" ) ),",","." )
		    If inspectionRS.Field( "inspections_radius2" ).DoubleValue = 0 Then inspection.Value( "inspections_radius2" ) = ""
		    inspection.Value( "inspections_radius2_ps" ) =  ReplaceAll( str( Format( inspectionRS.Field( "inspections_radius2_ps" ).DoubleValue, "#.00" ) ),",","." )
		    If inspectionRS.Field( "inspections_radius2_ps" ).DoubleValue = 0 Then inspection.Value( "inspections_radius2_ps" ) = ""
		    inspection.Value( "inspections_radius2_ss" ) =  ReplaceAll( str( Format( inspectionRS.Field( "inspections_radius2_ss" ).DoubleValue, "#.00" ) ),",","." )
		    If inspectionRS.Field( "inspections_radius2_ss" ).DoubleValue = 0 Then inspection.Value( "inspections_radius2_ss" ) = ""
		    inspection.Value( "inspections_radius3" ) =  ReplaceAll( str( Format( inspectionRS.Field( "inspections_radius3" ).DoubleValue, "#.00" ) ),",","." )
		    If inspectionRS.Field( "inspections_radius3" ).DoubleValue = 0 Then inspection.Value( "inspections_radius3" ) = ""
		    inspection.Value( "inspections_radius3_ps" ) =  ReplaceAll( str( Format( inspectionRS.Field( "inspections_radius3_ps" ).DoubleValue, "#.00" ) ),",","." )
		    If inspectionRS.Field( "inspections_radius3_ps" ).DoubleValue = 0 Then inspection.Value( "inspections_radius3_ps" ) = ""
		    inspection.Value( "inspections_radius3_ss" ) =  ReplaceAll( str( Format( inspectionRS.Field( "inspections_radius3_ss" ).DoubleValue, "#.00" ) ),",","." )
		    If inspectionRS.Field( "inspections_radius3_ss" ).DoubleValue = 0 Then inspection.Value( "inspections_radius3_ss" ) = ""
		    inspection.Value( "inspections_radius_drain_value" ) = ReplaceAll( str( Format( inspectionRS.Field( "inspections_radius_drain_value" ).DoubleValue, "#.00" ) ),",","." )
		    If inspectionRS.Field( "inspections_radius_drain_value" ).DoubleValue = 0 Then inspection.Value( "inspections_radius_drain_value" ) = ""
		    inspection.Value( "inspections_radius_drain_cleared" ) = inspectionRS.Field( "inspections_radius_drain_cleared" ).StringValue
		    inspection.Value( "last_synced" ) = inspectionRS.Field( "last_synced" ).IntegerValue
		    inspection.Value( "mobile_timestamp" ) = inspectionRS.Field( "mobile_timestamp" ).IntegerValue
		    inspection.Value( "unique_record_identifier" ) = inspectionRS.Field( "unique_record_identifier" ).StringValue
		    
		    'Set inspection user_id based on the  fact that the inspection is linked to an assignment performed by the user
		    inspection.Value( "user_id" ) = CStr( mUserID )
		    
		    jsonInspections.Value( inspectionRS.Field( "inspections_assignment_number" ).StringValue + " " + inspectionRS.Field( "inspections_inspection_num" ).StringValue ) = inspection
		    
		    Suivant:
		    inspectionRS.MoveNext
		  Wend
		  
		  inspectionRS.Close
		  inspectionRS = Nil
		  
		  Dim jsonResults As New JSONItem
		  jsonResults.Value( "getInspectionsAll" ) = jsonInspections
		  jsonResults.Value( "ReturnMessage" ) = App.kSuccess
		  
		  Return jsonResults
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Get_Assignment_Status(assignNum As String) As String
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  Dim strSQL As String = "SELECT assignments.assignments_status_id" + _
		  " FROM dbglobal.assignments" + _
		  " WHERE assignments_number = $1"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare(strSQL)
		  pgps.Bind(0, assignNum)
		  
		  Dim RS As RecordSet = pgps.SQLSelect
		  
		  If Not (RS.BOF And RS.EOF) And RS.RecordCount > 0 Then
		    Return RS.Field("assignments_status_id").StringValue
		  End If
		  
		  Return ""
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Get_Assignment_Year(insp_assign_num As String) As Integer
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  //*** L'année est inscrite automatiquement à la création du projet
		  
		  Dim strSQL As String = "SELECT assignments.assignments_year" + _
		  " FROM dbglobal.assignments" + _
		  " WHERE assignments_number = $1"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare(strSQL)
		  pgps.Bind(0, insp_assign_num)
		  
		  Dim RS As RecordSet = pgps.SQLSelect
		  
		  If Not (RS.BOF And RS.EOF) And RS.RecordCount > 0 Then
		    Return RS.Field("assignments_year").IntegerValue
		  End If
		  
		  Return 0
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Get_Blade_Serial(insp_proj_num As String, insp_num As String) As String
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  
		  Dim strSQL As String = "SELECT inspections.inspections_blade_serial" + _
		  " FROM inspection.inspections" + _
		  " WHERE inspections_project_number = $1" + _
		  " AND inspections_inspection_num = $2;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare(strSQL)
		  pgps.Bind(0, insp_proj_num)
		  pgps.Bind(1, insp_num)
		  
		  Dim RS As RecordSet = pgps.SQLSelect
		  
		  If Not (RS.BOF And RS.EOF) And RS.RecordCount > 0 Then
		    Return RS.Field("inspections_blade_serial").StringValue
		  End If
		  
		  Return ""
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Get_Damage_Updatable(uniqueID As String) As Boolean
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  Dim strSQL As String = "SELECT inspectiondamages.inspectiondamages_updatable" + _
		  " FROM inspection.inspectiondamages" + _
		  " WHERE unique_record_identifier = $1;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare(strSQL)
		  pgps.Bind(0, uniqueID)
		  
		  Dim RS As RecordSet = pgps.SQLSelect
		  
		  If Not (RS.BOF And RS.EOF) And RS.RecordCount > 0 Then
		    Return RS.Field("inspectiondamages_updatable").BooleanValue
		  Else
		    Return True
		  End If
		  
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Get_Project_Status(projNum As String) As String
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  Dim strSQL As String = "SELECT projects.projects_status_id" + _
		  " FROM dbglobal.projects" + _
		  " WHERE projects_number = $1"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare(strSQL)
		  pgps.Bind(0, projNum)
		  
		  Dim RS As RecordSet = pgps.SQLSelect
		  
		  If Not (RS.BOF And RS.EOF) And RS.RecordCount > 0 Then
		    Return RS.Field("projects_status_id").StringValue
		  End If
		  
		  Return ""
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Get_Project_Year(insp_proj_num As String) As Integer
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  //*** L'année est inscrite automatiquement à la création du projet
		  
		  Dim strSQL As String = "SELECT projects.projects_year" + _
		  " FROM dbglobal.projects" + _
		  " WHERE projects_number = $1"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare(strSQL)
		  pgps.Bind(0, insp_proj_num)
		  
		  Dim RS As RecordSet = pgps.SQLSelect
		  
		  If Not (RS.BOF And RS.EOF) And RS.RecordCount > 0 Then
		    Return RS.Field("projects_year").IntegerValue
		  End If
		  
		  Return 0
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Ownership_And_Sharing(projNum As String) As Boolean
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  Dim strSQL As String
		  
		  Select Case mCompanyRole
		  Case "Operator"
		    strSQL = "SELECT projects.projects_shared" + _
		    " FROM dbglobal.projects" + _
		    " WHERE projects_number = $1"+ _
		    " AND ((projects_operator_code = $2) AND ((projects_owner_code = $2) OR (projects_owner_code <> $2 AND projects_shared = TRUE)));"
		  Case "Supplier"
		    strSQL = "SELECT projects.projects_shared" + _
		    " FROM dbglobal.projects" + _
		    " WHERE projects_number = $1"+ _
		    " AND ((projects_supplier_code = $2 AND ((projects_owner_code = $2) OR (projects_owner_code <> $2 AND projects_shared = TRUE)));"
		  End Select
		  
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare(strSQL)
		  pgps.Bind(0, projNum)
		  pgps.Bind(1, mUsers.users_company_code)
		  
		  Dim RS As RecordSet = pgps.SQLSelect
		  
		  If Not (RS.BOF And RS.EOF) And RS.RecordCount > 0 Then
		    Return True
		  End If
		  
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UpdateInspection(inspectionDic As Xojo.Core.Dictionary) As Text
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  Dim inspUniqueID As String = inspectionDic.Lookup("unique_record_identifier","")
		  Dim strSQL As String = ""
		  
		  // Modification de l'enregistrements correspondants au projet et inspection
		  strSQL = "UPDATE inspections SET" + _
		  " inspections_progress_status = $2," + _
		  " inspections_start_date = $3," + _
		  " inspections_end_date = $4," + _
		  " inspections_time_spent = $5," + _
		  " inspections_radius1 = $6," + _
		  " inspections_radius1_ps = $7," + _
		  " inspections_radius1_ss = $8," + _
		  " inspections_radius2 = $9," + _
		  " inspections_radius2_ps = $10," + _
		  " inspections_radius2_ss = $11," + _
		  " inspections_radius3  = $12," + _
		  " inspections_radius3_ps = $13," + _
		  " inspections_radius3_ss = $14," + _
		  " inspections_radius_drain_value = $15," + _
		  " inspections_radius_drain_cleared = $16," + _
		  " inspections_remark = $17," + _
		  " last_synced = $18," + _
		  " mobile_timestamp = $19" + _
		  " WHERE unique_record_identifier = $1;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = dB.Prepare(strSQL)
		  pgps.Bind(0, inspUniqueID)
		  pgps.Bind(1, inspectionDic.Lookup("inspections_progress_status",""))
		  pgps.Bind(2, inspectionDic.Lookup("inspections_start_date",""))
		  pgps.Bind(3, inspectionDic.Lookup("inspections_end_date",""))
		  pgps.Bind(4, Format(CDbl(inspectionDic.Lookup("inspections_time_spent", "")),"0,00"))
		  pgps.Bind(5, Format(CDbl(inspectionDic.Lookup("inspections_radius1","")), "0,00"))
		  pgps.Bind(6, Format(CDbl(inspectionDic.Lookup("inspections_radius1_ps","")), "0,00"))
		  pgps.Bind(7, Format(CDbl(inspectionDic.Lookup("inspections_radius1_ss","")), "0,00"))
		  pgps.Bind(8, Format(CDbl(inspectionDic.Lookup("inspections_radius2","")), "0,00"))
		  pgps.Bind(9, Format(CDbl(inspectionDic.Lookup("inspections_radius2_ps","")), "0,00"))
		  pgps.Bind(10, Format(CDbl(inspectionDic.Lookup("inspections_radius2_ss","")), "0,00"))
		  pgps.Bind(11, Format(CDbl(inspectionDic.Lookup("inspections_radius3","")), "0,00"))
		  pgps.Bind(12, Format(CDbl(inspectionDic.Lookup("inspections_radius3_ps","")), "0,00"))
		  pgps.Bind(13, Format(CDbl(inspectionDic.Lookup("inspections_radius3_ss","")), "0,00"))
		  pgps.Bind(14, Format(CDbl(inspectionDic.Lookup("inspections_radius_drain_value","")), "0,00"))
		  pgps.Bind(15,inspectionDic.Lookup("inspections_radius_drain_cleared","2")) //Default is 2 (not cleared)
		  pgps.Bind(16, inspectionDic.Lookup("inspections_remark",""))
		  pgps.Bind(17, inspectionDic.Lookup("last_synced",""))
		  pgps.Bind(18, inspectionDic.Lookup("mobile_timestamp",""))
		  
		  dB.SQLExecute("BEGIN TRANSACTION")
		  pgps.SQLExecute
		  //Gestion Erreur
		  Dim message As Text
		  If dB.Error Then
		    dB.Rollback
		    message = db.ErrorMessage.ToText
		  Else
		    dB.Commit
		    message = App.kSuccess.ToText
		  End If
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UpdateInspectionDamage(damageDic As Xojo.Core.Dictionary) As Text
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  
		  
		  Dim mobileUUID As String = damageDic.Lookup( "inspectiondamages_mobile_uuid","" )
		  
		  // Mise à jour des enregistrements
		  Dim strSQL As String = "UPDATE inspection.inspectiondamages SET" + _
		  " inspectiondamages_area = $1," + _
		  " inspectiondamages_assignment_number = $2," + _
		  " inspectiondamages_damage_desc = $3," + _
		  " inspectiondamages_damage_num = $4," + _
		  " inspectiondamages_damage_radius = $5," + _
		  " inspectiondamages_damage_severity = $6," + _
		  " inspectiondamages_damage_type = $7," + _
		  " inspectiondamages_date = $8," + _
		  " inspectiondamages_inspection_num = $9," + _
		  " inspectiondamages_length = $10," + _
		  " inspectiondamages_mobile_uuid = $11," + _
		  " inspectiondamages_photo_desc01 = $12," + _
		  " inspectiondamages_photo_desc02 = $13," + _
		  " inspectiondamages_position_edge = $14," + _
		  " inspectiondamages_position_side = $15," + _
		  " inspectiondamages_remark = $16," + _
		  " inspectiondamages_section = $17," + _
		  " inspectiondamages_updatable = $18," + _
		  " inspectiondamages_web = $19," + _
		  " last_synced = $20," + _
		  " mobile_timestamp = $21" + _
		  " WHERE" + _
		  " unique_record_identifier = $22;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare( strSQL )
		  pgps.Bind( 0, CDbl( damageDic.Lookup( "inspectiondamages_area","" ) ) )
		  pgps.Bind( 1, damageDic.Lookup( "inspectiondamages_assignment_number","" ) )
		  pgps.Bind( 2, damageDic.Lookup( "inspectiondamages_damage_desc","" ) )
		  pgps.Bind( 3, Format( CDbl( damageDic.Lookup( "inspectiondamages_damage_num","" ) ), "00" ) )
		  pgps.Bind( 4, CDbl( damageDic.Lookup( "inspectiondamages_damage_radius","" ) ) )
		  pgps.Bind( 5, damageDic.Lookup( "inspectiondamages_damage_severity","" ) )
		  pgps.Bind( 6, damageDic.Lookup( "inspectiondamages_damage_type","" ) )
		  pgps.Bind( 7, damageDic.Lookup( "inspectiondamages_date","" ) )
		  pgps.Bind( 8, Format( CDbl( damageDic.Lookup( "inspectiondamages_inspection_num","" ) ), "000" ) )
		  pgps.Bind( 9, CDbl( damageDic.Lookup( "inspectiondamages_length","" ) ) )
		  pgps.Bind( 10, Trim(  damageDic.Lookup( "inspectiondamages_mobile_uuid","" ) ) )
		  pgps.Bind( 11, damageDic.Lookup( "inspectiondamages_photo_desc01","" ) )
		  pgps.Bind( 12, damageDic.Lookup( "inspectiondamages_photo_desc02","" ) )
		  pgps.Bind( 13, damageDic.Lookup( "inspectiondamages_position_edge","" ) )
		  pgps.Bind( 14, damageDic.Lookup( "inspectiondamages_position_side","" ) )
		  pgps.Bind( 15, damageDic.Lookup( "inspectiondamages_remark","" ) )
		  pgps.Bind( 16, damageDic.Lookup( "inspectiondamages_section","" ) )
		  pgps.Bind( 17, damageDic.Lookup( "inspectiondamages_updatable","True" ) )
		  pgps.Bind( 18, damageDic.Lookup( "inspectiondamages_web","" ) )
		  pgps.Bind( 19, CLong( damageDic.Lookup( "last_synced","" ) ) )
		  pgps.Bind( 20, CLong( damageDic.Lookup( "mobile_timestamp","" ) ) )
		  pgps.Bind( 21, damageDic.Lookup( "unique_record_identifier","" ) )
		  
		  dB.SQLExecute( "BEGIN TRANSACTION" )
		  pgps.SQLExecute
		  //Gestion Erreur
		  Dim message As Text
		  If dB.Error Then
		    dB.Rollback
		    message = dB.ErrorMessage.ToText
		  Else
		    dB.Commit
		    message = App.kSuccess.ToText
		  End If
		  
		  Dim assignNum As String = damageDic.Lookup( "inspectiondamages_assignment_number","" )
		  Dim damUniqueID As String = damageDic.Lookup( "unique_record_identifier","" )
		  
		  //Mettre à jour les photos
		  If message = App.kSuccess.ToText Then
		    Update_Damage_Photos( damageDic, assignNum, damUniqueID )
		  End If
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Update_Damage_Photos(dDic As Xojo.Core.Dictionary, assignNum As String, damUniqueID As String)
		  // Ajout des photos correspondant à la sélection
		  ' Vérifier les répertoires photo
		  ' Répertoire Photo Global
		  Dim folderPhoto As FolderItem = GetFolderItem("").Parent.Parent.Child("webapp").Child("Photos")
		  #If DebugBuild Then
		    folderPhoto = GetFolderItem("").Parent.Parent.Parent.Child("webapp").Child("Photos")
		  #Else
		    folderPhoto = GetFolderItem("").Parent.Parent.Child("webapp").Child("Photos")
		  #EndIf
		  
		  If Not folderPhoto.Exists Or Not folderPhoto.Directory Then
		    folderPhoto.CreateAsFolder
		  End If
		  
		  'Répertoire photo année du projet
		  Dim yearFolder As String = Str(Get_Assignment_Year(assignNum))
		  Dim folderPhotoYear As FolderItem = folderPhoto.Child(yearFolder)
		  If Not folderPhotoYear.Exists Or Not folderPhotoYear.Directory Then
		    folderPhotoYear.CreateAsFolder
		  End If
		  
		  
		  // Ajout ou remplacement des photos
		  
		  For i As Integer = 1 To 2
		    Dim newPic As Picture
		    Dim pKey As Text = "inspectiondamages_photo" + Format(i, "00").ToText
		    If dDic.HasKey(pKey) And dDic.Value(pKey) <> "" Then
		      Try
		        newPic = Picture.FromData(DecodeBase64(dDic.Value(pKey), Encodings.UTF8))
		      Catch e As UnsupportedFormatException
		        Continue
		      End Try
		      
		      If newPic <> Nil Then
		        Dim f As FolderItem
		        Dim nomPhotoFull As String = damUniqueID + "_P" + Format(i, "00") + ".jpeg"
		        Dim nomPhotoTN As String = damUniqueID + "_P" + Format(i, "00") + "_TN.jpeg"
		        
		        //Effacer les photos existantes
		        ' Photo full size
		        f = folderPhoto.Child(nomPhotoFull)
		        If f.Exists Then f.Delete
		        
		        ' Photo thumbnail
		        f = folderPhoto.Child(nomPhotoTN)
		        If f.Exists Then f.Delete
		        
		        
		        // Créer les photos
		        ' Photo full size
		        f = folderPhotoYear.Child(nomPhotoFull)
		        newPic.Save(f, Picture.SaveAsJPEG)
		        
		        ' Photo de grosseur Thumbnail
		        f = folderPhotoYear.Child(nomPhotoTN)
		        Dim imageTN As Picture = App.resizePicture(newPic, 520, 358)
		        imageTN.Save(f, Picture.SaveAsJPEG)
		        
		      End If
		    End If
		  Next i
		  
		  
		  
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		mInspection As InspectionsModule.InspectionsClass
	#tag EndProperty

	#tag Property, Flags = &h0
		mInspectionDamages As InspectionsModule.InspectionDamagesClass
	#tag EndProperty


	#tag Constant, Name = AUCUNEINSPECTION, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No inspection record in the database"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Aucune inspection dans la base de donn\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No inspection record in the database"
	#tag EndConstant

	#tag Constant, Name = kPhotoExtentionsList, Type = String, Dynamic = False, Default = \"jpeg\x2Cjpg\x2Cpng\x2Cbmp\x2Ctiff\x2Cgif", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
