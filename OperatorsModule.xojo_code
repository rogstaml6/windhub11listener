#tag Module
Protected Module OperatorsModule
	#tag Method, Flags = &h0
		Function getSitesAll() As JSONItem
		  mSites = Nil 
		  mSites = New SitesClass
		  
		  Dim sitesRS As RecordSet
		  sitesRS = mSites.loadData(App.bdWindhub, "sites", "sites_name")
		  
		  If sitesRS.RecordCount = 0 Then 
		    Dim jsonError As New JSONItem
		    jsonError.Value("ReturnMessage") = App.kSuccess
		    Return jsonError
		  End If
		  
		  Dim jsonSites As New JSONItem
		  Dim sites As Dictionary
		  
		  While Not sitesRS.EOF
		    sites = New Dictionary
		    sites.Value("sites_name") = sitesRS.Field("sites_name").StringValue
		    sites.Value("sites_address") = sitesRS.Field("sites_address").StringValue
		    sites.Value("sites_city") = sitesRS.Field("sites_city").StringValue
		    sites.Value("sites_state_prov") = sitesRS.Field("sites_state_prov").StringValue
		    sites.Value("sites_country") = sitesRS.Field("sites_country").StringValue
		    sites.Value("sites_manager_phone") = sitesRS.Field("sites_manager_phone").StringValue
		    sites.Value("sites_manager_name") = sitesRS.Field("sites_manager_name").StringValue
		    sites.Value("sites_geoposition") = sitesRS.Field("sites_geoposition").StringValue
		    sites.Value("sites_operator_code") = sitesRS.Field("sites_operator_code").StringValue
		    sites.Value("sites_status_id") = sitesRS.Field("sites_status_id").StringValue
		    sites.Value("sites_code") = sitesRS.Field("sites_code").StringValue
		    jsonSites.Value(str(sitesRS.Field("sites_id").IntegerValue)) = sites
		    sitesRS.MoveNext
		  Wend
		  
		  sitesRS.Close
		  sitesRS = Nil
		  
		  Dim jsonResults As New JSONItem
		  jsonResults.Value("getSitesAll") = jsonSites
		  jsonResults.Value("ReturnMessage") = App.kSuccess
		  
		  Return jsonResults
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		mSites As OperatorsModule.SitesClass
	#tag EndProperty


	#tag Constant, Name = NOSITEDB, Type = String, Dynamic = True, Default = \"No corresponding sites in the database", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
