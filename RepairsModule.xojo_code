#tag Module
Protected Module RepairsModule
	#tag Method, Flags = &h0
		Function AddRepairStep(stepDic As Xojo.Core.Dictionary) As Text
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  Dim mobileUUID As String = stepDic.Lookup( "repairsteps_mobile_uuid","" )
		  
		  // Mise à jour des enregistrements
		  Dim strSQL As String = "INSERT INTO repair.repairsteps" + _
		  " (  last_synced," + _ '$1
		  " mobile_timestamp," + _
		  " repairsteps_date," + _
		  " repairsteps_mobile_uuid," + _
		  " repairsteps_photodesc01," + _
		  " repairsteps_assignment_number," + _
		  " repairsteps_repair_num," + _
		  " repairsteps_step_num," + _
		  " repairsteps_updatable," + _ ' $9
		  " unique_record_identifier  )" + _
		  " VALUES(  $1, $2, $3, $4, $5, $6, $7, $8, $9, $10  ) ;"
		  
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare( strSQL )
		  pgps.Bind( 0, CLong( stepDic.Lookup( "last_synced","0" ) ) )
		  pgps.Bind( 1, CLong( stepDic.Lookup( "mobile_timestamp","" ) ) )
		  pgps.Bind( 2, stepDic.Lookup( "repairsteps_date","" ) )
		  pgps.Bind( 3, Trim( stepDic.Lookup( "repairsteps_mobile_uuid","" ) ) )
		  pgps.Bind( 4,  stepDic.Lookup( "repairsteps_photodesc01","" ) )
		  pgps.Bind( 5,  stepDic.Lookup( "repairsteps_assignment_number","" ) )
		  pgps.Bind( 6,  Format( CDbl( stepDic.Lookup( "repairsteps_repair_num","" ) ), "000" ) )
		  pgps.Bind( 7,  Format( CDbl( stepDic.Lookup( "repairsteps_step_num","" ) ), "00" ) )
		  pgps.Bind( 8,  stepDic.Lookup( "repairsteps_updatable","True" ) )
		  pgps.Bind( 9,  stepDic.Lookup( "unique_record_identifier","" ) )
		  
		  dB.SQLExecute( "BEGIN TRANSACTION" )
		  pgps.SQLExecute
		  //Gestion Erreur
		  Dim message As Text
		  If dB.Error Then
		    dB.Rollback
		    message = dB.ErrorMessage.ToText
		  Else
		    dB.Commit
		    message = App.kSuccess.ToText
		  End If
		  
		  
		  Dim projNum As String = stepDic.Lookup( "repairsteps_assignment_number","" )
		  Dim repUniqueID As String =stepDic.Lookup( "unique_record_identifier","" )
		  
		  //Mettre à jour les photos
		  If message = App.kSuccess.ToText Then
		    UpdateRepairStepsPhotos( stepDic, projNum, repUniqueID )
		  End If
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Delete_Steps(assignNum As String, repUniqueID As String) As Text
		  Dim dB As PostgreSQLDatabase = App.bdWindhub
		  Dim strSQL As String
		  
		  strSQL = "DELETE FROM repair.repairsteps" + _
		  " WHERE unique_record_identifier = $1" + _
		  " AND repairsteps_updatable = TRUE;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = dB.Prepare(strSQL)
		  pgps.Bind(0, repUniqueID)
		  
		  dB.SQLExecute("BEGIN TRANSACTION")
		  pgps.SQLExecute
		  //Gestion Erreur
		  Dim message As Text
		  If dB.Error Then
		    message = dB.ErrorMessage.ToText
		    dB.Rollback
		  Else
		    dB.Commit
		    message = App.kSuccess.ToText
		  End If
		  
		  Return message
		  
		  
		  /// Destruction des photos correspondant à la sélection
		  //// Déterminer le répertoire de la photo
		  //Dim yearPhoto As String = Str(Get_Project_Year(assignNim))
		  //Dim folderPhoto As FolderItem
		  //
		  //#If DebugBuild Then
		  //folderPhoto = GetFolderItem("").Parent.Parent.Parent.Child("webapp").Child("Photos").Child(yearPhoto)
		  //#Else
		  //folderPhoto = GetFolderItem("").Parent.Parent.Child("webapp").Child("Photos").Child(yearPhoto)
		  //#EndIf
		  //
		  //If Not folderPhoto.Exists Then
		  //folderPhoto.CreateAsFolder
		  //End If
		  //
		  //// Destruction des photos
		  //Dim f As FolderItem
		  //Dim nomPhoto As String = RS.Field("repairs_blade_serial").StringValue + "_" + RS.Field("repairsteps_assignment_number").StringValue + "_" + _
		  //RS.Field("repairsteps_repair_num").StringValue + "_" + RS.Field("repairsteps_step_num").StringValue
		  //
		  //Dim nomPhotoTrav As String = nomPhoto + "_P01.jpeg"
		  //f = folderPhoto.Child(nomPhotoTrav)
		  //If f.Exists Then f.Delete
		  ////nomPhotoTrav = nomPhoto + "_P02.jpeg"
		  ////f = folderPhoto.Child(nomPhotoTrav)
		  ////If f.Exists Then f.Delete
		  //nomPhotoTrav = nomPhoto + "_P01_TN.jpeg"
		  //f = folderPhoto.Child(nomPhotoTrav)
		  //If f.Exists Then f.Delete
		  ////nomPhotoTrav = nomPhoto + "_P02_TN.jpeg"
		  ////f = folderPhoto.Child(nomPhotoTrav)
		  ////If f.Exists Then f.Delete
		  //
		  //RS.MoveNext
		  //Wend
		  //End If
		  //Next i
		  //Next entry
		  //
		  //
		  //// Suppression des enregistrements correspondants au projet, inspection et dommage, qui sont modifiables et qui correspondent à l'appareil mobile
		  //For Each entry As Xojo.Core.DictionaryEntry In active_projects
		  //Dim arrRepNum() As String = entry.Value
		  //For i As Integer = 0 To arrRepNum.Ubound
		  //strSQL = "DELETE FROM repair.repaisteps WHERE repairsteps_assignment_number = $1" + _
		  //" AND repairsteps_repair_num = $2" + _
		  //" AND repairsteps_updatable = TRUE"
		  //
		  //Dim assignNim As String = entry.Key
		  ////Dim arri As String =  arrRepNum(i)
		  //Dim repNum As String = Format(Val(arrRepNum(i)), "000")
		  //
		  //Dim pgps As PostgreSQLPreparedStatement = dB.Prepare(strSQL)
		  //pgps.Bind(0, assignNim)
		  //pgps.Bind(1, repNum)
		  //
		  //dB.SQLExecute("BEGIN TRANSACTION")
		  //pgps.SQLExecute
		  ////Gestion Erreur
		  //If dB.Error Then
		  //Dim messageErreur As String = dB.ErrorMessage.ToText
		  //dB.Rollback
		  //Return messageErreur
		  //Else
		  //dB.Commit
		  //
		  //End If
		  //
		  //Next i
		  //Next entry
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getRepairsAll(inputDataDict_param As Dictionary) As JSONItem
		  mRepair = Nil 
		  mRepair = New RepairsClass
		  
		  Dim repairRS As RecordSet
		  repairRS = mRepair.loadData(App.bdWindhub, "repairs", "repairs_assignment_number", "repairs_repair_num")
		  //Dim assignmentDict As Dictionary = inputDataJson_param.Value("getassignmentData")
		  Dim jsonRepairs As New JSONItem
		  Dim repair As Dictionary
		  
		  If repairRS.RecordCount = 0 Or inputDataDict_param.Count  = 0 Then 
		    Dim jsonError As New JSONItem
		    jsonError.Value( "ReturnMessage" ) = RepairsModule.AUCUNEREPARATION
		    jsonError.Value( "getRepairsAll" ) = ""
		    Return jsonError
		  End If
		  
		  While Not repairRS.EOF
		    
		    Dim assignNum As String = repairRS.Field( "repairs_assignment_number" ).StringValue
		    Dim repairNum As String = repairRS.Field( "repairs_repair_num" ).StringValue
		    
		    If Not inputDataDict_param.HasKey( assignNum + " " + repairNum ) Then GoTo Suivant
		    
		    repair = New Dictionary
		    repair.Value("repairs_assignment_number") = repairRS.Field("repairs_assignment_number").StringValue
		    repair.Value("repairs_repair_num") = repairRS.Field("repairs_repair_num").StringValue
		    repair.Value("repairs_wtg_num") = repairRS.Field("repairs_wtg_num").StringValue
		    repair.Value("repairs_blade_num") = repairRS.Field("repairs_blade_num").StringValue
		    repair.Value("repairs_blade_serial") = repairRS.Field("repairs_blade_serial").StringValue
		    repair.Value("repairs_damage_type") = repairRS.Field("repairs_damage_type").StringValue
		    repair.Value("repairs_damage_desc") = repairRS.Field("repairs_damage_desc").StringValue
		    repair.Value("repairs_damage_severity") = repairRS.Field("repairs_damage_severity").StringValue
		    repair.Value("repairs_inspection_damage_id") = repairRS.Field("repairs_inspection_damage_id").StringValue
		    repair.Value("repairs_int_ext") = repairRS.Field("repairs_int_ext").StringValue
		    repair.Value("repairs_access_method") = repairRS.Field("repairs_access_method").StringValue
		    repair.Value("repairs_priority") = repairRS.Field("repairs_priority").StringValue
		    repair.Value("repairs_progress_status") = repairRS.Field("repairs_progress_status").StringValue
		    repair.Value("repairs_completion_status") = repairRS.Field("repairs_completion_status").StringValue
		    repair.Value("repairs_position_side") = repairRS.Field("repairs_position_side").StringValue
		    repair.Value("repairs_position_edge") = repairRS.Field("repairs_position_edge").StringValue
		    repair.Value("repairs_radius") = ReplaceAll(Str(Format(repairRS.Field("repairs_radius").DoubleValue, "#.00")),",",".")
		    If repairRS.Field("repairs_radius").DoubleValue = 0 Then repair.Value("repairs_radius") = ""
		    repair.Value("repairs_length") = ReplaceAll(Str(Format(repairRS.Field("repairs_length").DoubleValue, "#.00")),",",".")
		    If repairRS.Field("repairs_length").DoubleValue = 0 Then repair.Value("repairs_length") = ""
		    repair.Value("repairs_area") = ReplaceAll(Str(Format(repairRS.Field("repairs_area").DoubleValue, "#.00")),",",".")
		    If repairRS.Field("repairs_area").DoubleValue = 0 Then repair.Value("repairs_area") = ""
		    repair.Value("repairs_start_date") = repairRS.Field("repairs_start_date").StringValue
		    repair.Value("repairs_end_date") = repairRS.Field("repairs_end_date").StringValue
		    repair.Value("repairs_balsa_affected") = repairRS.Field("repairs_balsa_affected").StringValue
		    repair.Value("repairs_nb_laminate_affected") = repairRS.Field("repairs_nb_laminate_affected").StringValue
		    repair.Value("repairs_nb_laminate_balsa") = repairRS.Field("repairs_nb_laminate_balsa").StringValue
		    repair.Value("repairs_cover") = repairRS.Field("repairs_cover").StringValue
		    repair.Value("repairs_time_estimated") = ReplaceAll(Str(Format(repairRS.Field("repairs_time_estimated").DoubleValue, "#.00")),",",".")
		    If repairRS.Field("repairs_time_estimated").DoubleValue = 0 Then repair.Value("repairs_time_estimated") = ""
		    repair.Value("repairs_time_spent") = ReplaceAll(Str(Format(repairRS.Field("repairs_time_spent").DoubleValue, "#.00")),",",".")
		    If repairRS.Field("repairs_time_spent").DoubleValue = 0 Then repair.Value("repairs_time_spent") = ""
		    repair.Value("repairs_remark") = repairRS.Field("repairs_remark").StringValue
		    repair.Value("repairs_wtg_id") = Str(repairRS.Field("repairs_wtg_id").IntegerValue)
		    repair.Value("unique_record_identifier") = repairRS.Field("unique_record_identifier").StringValue
		    repair.Value("mobile_timestamp") = repairRS.Field("mobile_timestamp").IntegerValue
		    repair.Value("last_synced") = repairRS.Field("last_synced").IntegerValue
		    
		    'Set inspection user_id based on the  fact that the inspection is linked to an assignment performed by the user
		    repair.Value( "user_id" ) = CStr( mUserID )
		    
		    jsonRepairs.Value(repairRS.Field("repairs_assignment_number").StringValue + " " + repairRS.Field("repairs_repair_num").StringValue) = repair
		    Suivant:
		    repairRS.MoveNext
		  Wend
		  
		  
		  repairRS.Close
		  repairRS = Nil
		  
		  Dim jsonResults As New JSONItem
		  jsonResults.Value("getRepairsAll") = jsonRepairs
		  jsonResults.Value("ReturnMessage") = App.kSuccess
		  
		  Return jsonResults
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getRepairStepsAll(inputDataDict_param As Dictionary) As JSONItem
		  Dim S As Session = Session
		  mRepairSteps = Nil 
		  mRepairSteps = New RepairStepsClass
		  
		  Dim repairStepsRS As RecordSet
		  repairStepsRS = mRepairSteps.loadData(App.bdWindhub, "repairsteps", "repairsteps_assignment_number", "repairsteps_repair_num", "repairsteps_step_num")
		  
		  If repairStepsRS.RecordCount = 0 Then 
		    Dim jsonError As New JSONItem
		    jsonError.Value("ReturnMessage") = RepairsModule.AUCUNEETAPEREPARATION
		    jsonError.Value("getRepairStepsAll") = ""
		    Return jsonError
		  End If
		  
		  Dim jsonRepairSteps As New JSONItem
		  Dim repairSteps As Dictionary
		  
		  While Not repairStepsRS.EOF
		    
		    Dim assignNum As String = repairStepsRS.Field( "repairsteps_assignment_number" ).StringValue
		    Dim repairNum As String = repairStepsRS.Field( "repairsteps_repair_num" ).StringValue
		    
		    If Not inputDataDict_param.HasKey( assignNum + " " + repairNum ) Then GoTo Suivant
		    
		    repairSteps = New Dictionary
		    repairSteps.Value("repairsteps_assignment_number") = repairStepsRS.Field("repairsteps_assignment_number").StringValue
		    repairSteps.Value("repairsteps_repair_num") = repairStepsRS.Field("repairsteps_repair_num").StringValue
		    repairSteps.Value("repairsteps_step_num") = repairStepsRS.Field("repairsteps_step_num").StringValue
		    repairSteps.Value("repairsteps_date") = repairStepsRS.Field("repairsteps_date").StringValue
		    repairSteps.Value("repairsteps_photodesc01") = repairStepsRS.Field("repairsteps_photodesc01").StringValue
		    repairSteps.Value("unique_record_identifier") = repairStepsRS.Field("unique_record_identifier").StringValue
		    repairSteps.Value("mobile_timestamp") = repairStepsRS.Field("mobile_timestamp").IntegerValue
		    repairSteps.Value("last_synced") = repairStepsRS.Field("last_synced").IntegerValue
		    
		    'Set inspection user_id based on the  fact that the inspection is linked to an assignment performed by the user
		    repairSteps.Value( "user_id" ) = CStr( mUserID )
		    
		    jsonRepairSteps.Value(repairStepsRS.Field("repairsteps_assignment_number").StringValue + " " + repairStepsRS.Field("repairsteps_repair_num").StringValue + _
		    " " + repairStepsRS.Field("repairsteps_step_num").StringValue) = repairSteps
		    Suivant:
		    repairStepsRS.MoveNext
		  Wend
		  
		  repairStepsRS.Close
		  repairStepsRS = Nil
		  
		  Dim jsonResults As New JSONItem
		  jsonResults.Value("getRepairStepsAll") = jsonRepairSteps
		  jsonResults.Value("ReturnMessage") = App.kSuccess
		  
		  Return jsonResults
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function insertRepairSteps(repairStepsDic As Dictionary, assignmentNumber_param As String, repairNumber_param As String, stepNumber_param As String) As DatabaseRecord
		  Dim S As Session = Session
		  
		  Dim row As New DatabaseRecord
		  
		  
		  For Each key As String In mRepairSteps.propertyDict.Keys
		    Dim prop As Introspection.PropertyInfo = mRepairSteps.propertyDict.Value(key)
		    Dim propName As String = prop.Name
		    Dim variantProp As Variant = repairStepsDic.Lookup(propName,"")
		    
		    Select Case prop.PropertyType.FullName
		      
		    Case "Boolean"
		      row.BooleanColumn(key) = variantProp.BooleanValue
		    Case "Currency"
		      row.CurrencyColumn(key) =variantProp.CurrencyValue
		    Case "Date"
		      row.DateColumn(key) =variantProp.DateValue
		    Case "Double"
		      row.DoubleColumn(key) = variantProp.DoubleValue
		    Case "Integer"
		      row.IntegerColumn(key) = variantProp.IntegerValue
		    Case "Int32"
		      row.IntegerColumn(key) = variantProp.IntegerValue
		    Case "Int64"
		      row.Int64Column(key) = variantProp.Int64Value
		    Case "Picture"
		      row.PictureColumn(key) = variantProp
		    Case "String"
		      row.Column(key) =variantProp.StringValue
		    End Select
		    
		  Next
		  
		  return row
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function stepExists(stepUniqueID As String) As Boolean
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  
		  Dim strSQL As String = "SELECT count(unique_record_identifier)" + _
		  " FROM repair.repairsteps" + _
		  " WHERE unique_record_identifier = $1;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare(strSQL)
		  pgps.Bind(0, stepUniqueID)
		  
		  Dim RS As RecordSet = pgps.SQLSelect
		  
		  If Not (RS.BOF And RS.EOF) And RS.RecordCount > 0 Then
		    
		    Select Case RS.Field("count").IntegerValue
		    Case Is = 0
		      Return False
		    Case Is > 0
		      Return True
		    End Select
		    
		  End If
		  
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UpdateRepair(repairDic As Xojo.Core.Dictionary) As Text
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  Dim repUniqueID As String = repairDic.Lookup("unique_record_identifier","")
		  
		  Dim strSQL As String = ""
		  
		  // Modification de l'enregistrements correspondants au projet et réparation
		  strSQL = "UPDATE repairs SET" + _
		  " repairs_blade_serial = $2," + _
		  " repairs_damage_type = $3," + _
		  " repairs_damage_desc = $4," + _
		  " repairs_damage_severity = $5," + _
		  " repairs_int_ext = $6," + _
		  " repairs_access_method = $7," + _
		  " repairs_priority = $8," + _
		  " repairs_progress_status = $9," + _
		  " repairs_completion_status = $10," + _
		  " repairs_position_side = $11," + _
		  " repairs_position_edge = $12," + _
		  " repairs_radius = $13," + _
		  " repairs_length = $14," + _
		  " repairs_area = $15," + _
		  " repairs_start_date = $16," + _
		  " repairs_end_date = $17," + _
		  " repairs_balsa_affected = $18," + _
		  " repairs_nb_laminate_affected = $19," + _
		  " repairs_nb_laminate_balsa = $20," + _
		  " repairs_cover = $21," + _
		  " repairs_time_estimated = $22," + _
		  " repairs_time_spent = $23," + _
		  " repairs_remark = $24," + _
		  " last_synced = $25," + _
		  " mobile_timestamp = $26" + _
		  " WHERE unique_record_identifier = $1;"
		  Dim pgps As PostgreSQLPreparedStatement = dB.Prepare(strSQL)
		  pgps.Bind(0, repUniqueID)
		  pgps.Bind(1, repairDic.Lookup("repairs_blade_serial",""))
		  pgps.Bind(2, repairDic.Lookup("repairs_damage_type",""))
		  pgps.Bind(3, repairDic.Lookup("repairs_damage_desc",""))
		  pgps.Bind(4, repairDic.Lookup("repairs_damage_severity",""))
		  pgps.Bind(5, repairDic.Lookup("repairs_int_ext",""))
		  pgps.Bind(6, repairDic.Lookup("repairs_access_method",""))
		  pgps.Bind(7, repairDic.Lookup("repairs_priority",""))
		  pgps.Bind(8, repairDic.Lookup("repairs_progress_status",""))
		  pgps.Bind(9, repairDic.Lookup("repairs_completion_status",""))
		  pgps.Bind(10, repairDic.Lookup("repairs_position_side",""))
		  pgps.Bind(11, repairDic.Lookup("repairs_position_edge",""))
		  pgps.Bind(12, repairDic.Lookup("repairs_radius",""))
		  pgps.Bind(13, repairDic.Lookup("repairs_length",""))
		  pgps.Bind(14, repairDic.Lookup("repairs_area",""))
		  pgps.Bind(15, repairDic.Lookup("repairs_start_date",""))
		  pgps.Bind(16, repairDic.Lookup("repairs_end_date",""))
		  pgps.Bind(17, repairDic.Lookup("repairs_balsa_affected",""))
		  pgps.Bind(18, repairDic.Lookup("repairs_nb_laminate_affected",""))
		  pgps.Bind(19, repairDic.Lookup("repairs_nb_laminate_balsa",""))
		  pgps.Bind(20, repairDic.Lookup("repairs_cover",""))
		  pgps.Bind(21, Format(CDbl(repairDic.Lookup("repairs_time_estimated", "")),"0,00"))
		  pgps.Bind(22, Format(CDbl(repairDic.Lookup("repairs_time_spent", "")),"0,00"))
		  pgps.Bind(23, repairDic.Lookup("repairs_remark",""))
		  pgps.Bind(24, repairDic.Lookup("last_synced",""))
		  pgps.Bind(25, repairDic.Lookup("mobile_timestamp",""))
		  
		  dB.SQLExecute("BEGIN TRANSACTION")
		  pgps.SQLExecute
		  //Gestion Erreur
		  Dim message As Text
		  If dB.Error Then
		    db.Rollback
		    message = db.ErrorMessage.ToText
		  Else
		    dB.Commit
		    message = App.kSuccess.ToText
		  End If
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UpdateRepairStep(stepDic As Xojo.Core.Dictionary) As Text
		  Dim db As PostgreSQLDatabase = App.bdWindhub
		  
		  Dim mobileUUID As String = stepDic.Lookup("repairsteps_mobile_uuid","")
		  
		  // Mise à jour des enregistrements
		  Dim strSQL As String = "UPDATE repair.repairsteps SET" + _
		  " last_synced = $1," + _
		  " mobile_timestamp = $2," + _
		  " repairsteps_date = $3," + _
		  " repairsteps_mobile_uuid = $4," + _
		  " repairsteps_photodesc01 = $5," + _
		  " repairsteps_assignment_number = $6," + _
		  " repairsteps_repair_num = $7," + _
		  " repairsteps_step_num = $8," + _
		  " repairsteps_updatable = $9" + _
		  " WHERE unique_record_identifier = $10;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare(strSQL)
		  pgps.Bind(0, CLong(stepDic.Lookup("last_synced","0")))
		  pgps.Bind(1, CLong(stepDic.Lookup("mobile_timestamp","")))
		  pgps.Bind(2, stepDic.Lookup("repairsteps_date",""))
		  pgps.Bind(3, Trim(stepDic.Lookup("repairsteps_mobile_uuid","")))
		  pgps.Bind(4,  stepDic.Lookup("repairsteps_photodesc01",""))
		  pgps.Bind(5,  stepDic.Lookup("repairsteps_assignment_number",""))
		  pgps.Bind(6,  Format( CDbl( stepDic.Lookup("repairsteps_repair_num","")), "000" ) )
		  pgps.Bind(7,  Format( CDbl( stepDic.Lookup("repairsteps_step_num","")), "00" ) )
		  pgps.Bind(8,  stepDic.Lookup("repairsteps_updatable","True"))
		  pgps.Bind(9,  stepDic.Lookup("unique_record_identifier",""))
		  
		  dB.SQLExecute("BEGIN TRANSACTION")
		  pgps.SQLExecute
		  //Gestion Erreur
		  Dim message As Text
		  If dB.Error Then
		    dB.Rollback
		    message = dB.ErrorMessage.ToText
		  Else
		    dB.Commit
		    message = App.kSuccess.ToText
		  End If
		  
		  
		  Dim assignNum As String = stepDic.Lookup("repairsteps_assignment_number","")
		  Dim repUniqueID As String =stepDic.Lookup("unique_record_identifier","")
		  
		  //Mettre à jour les photos
		  If message = App.kSuccess.ToText Then
		    UpdateRepairStepsPhotos(stepDic, assignNum, repUniqueID)
		  End If
		  
		  Return message
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateRepairStepsPhotos(sDic As Xojo.Core.Dictionary, assignNum As String, repUniqueID As String)
		  
		  // Ajout des photos correspondant à la sélection
		  ' Vérifier les répertoires photo
		  ' Répertoire Photo Global
		  Dim folderPhoto As FolderItem = GetFolderItem("").Parent.Parent.Child("webapp").Child("Photos")
		  #If DebugBuild Then
		    folderPhoto = GetFolderItem("").Parent.Parent.Parent.Child("webapp").Child("Photos")
		  #Else
		    folderPhoto = GetFolderItem("").Parent.Parent.Child("webapp").Child("Photos")
		  #EndIf
		  
		  If Not folderPhoto.Exists Or Not folderPhoto.Directory Then
		    folderPhoto.CreateAsFolder
		  End If
		  
		  'Répertoire photo année du projet
		  Dim yearFolder As String = Str(Get_Assignment_Year(assignNum))
		  Dim folderPhotoYear As FolderItem = folderPhoto.Child(yearFolder)
		  If Not folderPhotoYear.Exists Or Not folderPhotoYear.Directory Then
		    folderPhotoYear.CreateAsFolder
		  End If
		  
		  // Ajout ou remplacement des photos
		  For i As Integer = 1 To 1
		    Dim newPic As Picture
		    Dim pKey As Text = "repairsteps_photo" ' + Format(i, "00").ToText
		    If sDic.HasKey(pKey) And sDic.Value(pKey) <> "" Then
		      Try
		        newPic = Picture.FromData(DecodeBase64(sDic.Value(pKey), Encodings.UTF8))
		      Catch e As UnsupportedFormatException
		        Exit
		      End Try
		      
		      If newPic <> Nil Then
		        Dim f As FolderItem
		        Dim nomPhotoFull As String = repUniqueID + "_P01.jpeg"
		        Dim nomPhotoTN As String = repUniqueID + "_P01_TN.jpeg"
		        
		        //Effacer les photos existantes
		        ' Photo full size
		        f = folderPhoto.Child(nomPhotoFull)
		        If f.Exists Then f.Delete
		        
		        ' Photo thumbnail
		        f = folderPhoto.Child(nomPhotoTN)
		        If f.Exists Then f.Delete
		        
		        
		        // Créer les photos
		        ' Photo full size
		        f = folderPhotoYear.Child(nomPhotoFull)
		        newPic.Save(f, Picture.SaveAsJPEG)
		        
		        ' Photo de grosseur Thumbnail
		        f = folderPhotoYear.Child(nomPhotoTN)
		        Dim imageTN As Picture = App.resizePicture(newPic, 520, 358)
		        imageTN.Save(f, Picture.SaveAsJPEG)
		        
		      End If
		      
		    End 
		    
		  Next i
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		mRepair As RepairsModule.RepairsClass
	#tag EndProperty

	#tag Property, Flags = &h0
		mRepairSteps As RepairsModule.RepairStepsClass
	#tag EndProperty


	#tag Constant, Name = AUCUNEETAPEREPARATION, Type = String, Dynamic = True, Default = \"Aucune \xC3\xA9tape de r\xC3\xA9paration dans la base de donn\xC3\xA9es", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Aucune \xC3\xA9tape de r\xC3\xA9paration dans la base de donn\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Aucune \xC3\xA9tape de r\xC3\xA9paration dans la base de donn\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No repair steps record in the database"
	#tag EndConstant

	#tag Constant, Name = AUCUNEREPARATION, Type = String, Dynamic = True, Default = \"Aucune r\xC3\xA9paration dans la base de donn\xC3\xA9es", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Aucune r\xC3\xA9paration dans la base de donn\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Aucune r\xC3\xA9paration dans la base de donn\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No repair record in the database"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
