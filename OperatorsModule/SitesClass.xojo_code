#tag Class
Protected Class SitesClass
Inherits GenInterfDBClass
	#tag Property, Flags = &h0
		sites_address As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sites_city As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sites_code As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sites_country As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sites_geoposition As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sites_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		sites_manager_name As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sites_manager_phone As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sites_name As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sites_operator_code As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sites_state_prov As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sites_status_id As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites_address"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites_city"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites_country"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites_geoposition"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites_manager_name"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites_manager_phone"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites_name"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites_operator_code"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites_state_prov"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites_status_id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites_code"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
