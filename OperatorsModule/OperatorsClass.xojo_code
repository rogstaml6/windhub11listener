#tag Class
Protected Class OperatorsClass
Inherits GenInterfDBClass
	#tag Property, Flags = &h0
		operators_code As String
	#tag EndProperty

	#tag Property, Flags = &h0
		operators_contact As String
	#tag EndProperty

	#tag Property, Flags = &h0
		operators_contact_email As String
	#tag EndProperty

	#tag Property, Flags = &h0
		operators_contact_mobile As String
	#tag EndProperty

	#tag Property, Flags = &h0
		operators_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		operators_is_suscriber As String
	#tag EndProperty

	#tag Property, Flags = &h0
		operators_name As String
	#tag EndProperty

	#tag Property, Flags = &h0
		operators_status_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		operators_visible_to_others As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="operators_code"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="operators_contact"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="operators_contact_email"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="operators_contact_mobile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="operators_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="operators_is_suscriber"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="operators_name"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="operators_status_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="operators_visible_to_others"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
