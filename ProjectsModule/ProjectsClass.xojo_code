#tag Class
Protected Class ProjectsClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h0
		Sub Constructor(db As PostgresqlDatabase, company_code As String, userID As Integer)
		  Dim strSQL As String = "SELECT *, assignments_users_user_id AS user_id" + _
		  " FROM dbglobal.all_projects_and_assignments_for_filtering_by_user_view" + _
		  " WHERE ( ( projects_owner_code = $1 ) Or ( (  projects_owner_code <> $1 ) AND" + _
		  " ( assignments_operator_code = $1 OR assignments_supplier_code = $1 ) AND assignments_shared = TRUE ) )" + _
		  " AND assignments_users_user_id = $2;"
		  
		  Dim pgps As PostgreSQLPreparedStatement = db.Prepare( strSQL )
		  pgps.Bind( 0, company_code )
		  pgps.Bind( 1, userID ) 
		  
		  If Not db.Error Then
		    projectsRS = pgps.SQLSelect
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Count_Assignments_Per_Project(projNo As String, userID As Integer) As Integer
		  
		  Dim strSQL As String = "SELECT count(*)" + _
		  " FROM(" + _
		  " SELECT assignments_number, assignments_users_user_id, assignments_status_id" + _
		  " FROM dbglobal.all_projects_and_assignments_for_filtering_by_user_view" + _
		  " JOIN (SELECT DISTINCT (inspections_assignment_number)" + _
		  " FROM inspection.inspections) As insplist " + _
		  " ON assignments_number = insplist.inspections_assignment_number" + _
		  " WHERE projects_number = $1" + _
		  " AND assignments_users_user_id = $2" + _
		  " AND assignments_status_id = 3" + _
		  " UNION" + _
		  " SELECT assignments_number, assignments_users_user_id, assignments_status_id" + _
		  " FROM dbglobal.all_projects_and_assignments_for_filtering_by_user_view" + _
		  " JOIN (SELECT DISTINCT (repairs_assignment_number)" + _
		  " FROM repair.repairs) As replist" + _
		  " ON assignments_number = replist.repairs_assignment_number" + _
		  " WHERE projects_number = $1" + _
		  " AND assignments_users_user_id = $2" + _
		  " AND assignments_status_id = 3) AS unioninsprep;" 
		  
		  Dim pgps As PostgreSQLPreparedStatement = App.bdWindhub.Prepare( strSQL )
		  pgps.Bind( 0, projNo )
		  pgps.Bind( 1, userID )
		  
		  
		  Dim RS As RecordSet = pgps.SQLSelect
		  
		  If RS <> Nil Then
		    Return RS.Field( "count" ).IntegerValue
		  End If
		  
		  Return 0
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		projectsRS As RecordSet
	#tag EndProperty

	#tag Property, Flags = &h0
		projects_created_by_user_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		projects_desc As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projects_effective_end_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projects_effective_start_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projects_expected_end_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projects_expected_start_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projects_name As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projects_number As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projects_owner_code As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projects_shared As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		projects_site_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		projects_status_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		projects_type_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		user_id As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projects_created_by_user_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projects_desc"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projects_effective_end_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projects_effective_start_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projects_expected_end_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projects_expected_start_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projects_name"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projects_number"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projects_owner_code"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projects_shared"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projects_site_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projects_status_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projects_type_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="user_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
